<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = htmlspecialchars($_POST['name']);
    $email = htmlspecialchars($_POST['email']); // XSS protection

    echo "Name: " . $name . "<br>";
    echo "Email: " . $email;
}
?>
