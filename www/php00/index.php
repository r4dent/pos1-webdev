<?php
    //var_dump($_POST);
    if (isset($_POST['email']) && checkEmail($_POST['email'])){
        $email = $_POST['email'];
        echo "Validierung OK: ";
        echo htmlspecialchars($email);
        //var_dump(checkEmail($_POST['email']));
    } else {
        // TODO Fehlermeldung
        $email = "";
        echo "Validierung nicht OK: ";
        echo htmlspecialchars($email);
    }

    function checkEmail($emailCandidate){
        //var_dump($emailCandidate);
        $pattern = '/^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/m';
        if (preg_match($pattern, $emailCandidate) === 1){
            return true;
        } return false;
    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/index.js"></script>
    <title>PHP Test Vorbereitung</title>
</head>
<body>
<form action="" method="post" class="form-example">
  
  <div class="form-example">
    <label for="vname">Vorname: </label>
    <input type="text" name="vorname" id="vname" placeholder="Patrick" required />
  </div>
  <div class="form-example">
    <label for="nname">Nachname: </label>
    <input type="text" name="nachname" id="nname" placeholder="Mairhofer" required />
  </div>
  <div class="form-example">
    <label for="email">Enter your email: </label>
    <input type="email" value="<?php if (isset($_POST['email'])) {echo htmlspecialchars($_POST['email']);} ?>" name="email" id="email" required />
  </div>
  <div class="form-example">
    <label for="date">Datum: </label>
    <input type="date" name="date" id="date" required />
  </div>
  <div class="form-example">
    <input type="submit" value="Registrieren" />
  </div>

</form>
</body>
</html>