<?php

$errors = [];

function validate($name, $date, $height, $weight)
{
    return validateName($name) & validateDate($date) &
        validateHeight($height) & validateWeight($weight);
}

function validateName($name)
{
    global $errors;

    if (strlen($name) == 0) {
        $errors['name'] = "Name darf nicht leer sein";
        return false;
    } else if (strlen($name) >= 25) {
        $errors['name'] = "Name zu lang";
        return false;
    } else {
        return true;
    }
}


function validateHeight($height)
{
    global $errors;

    if (!is_numeric($height) || $height < 1 || $height >= 300) {
        $errors['height'] = "Größe in cm ist ungültig";
        return false;
    } else {
        return true;
    }
}

function validateWeight($weight)
{
    global $errors;

    if (!is_numeric($weight) || $weight < 1 || $weight >= 500) {
        $errors['weight'] = "Gewicht in Kg ist ungültig";
        return false;
    } else {
        return true;
    }
}

function validatedate($date)
{
    global $errors;

    try {
        if ($date == "") {
            $errors['date'] = "Datum darf nicht leer sein";
            return false;
        } else if (new DateTime($date) > new DateTime()) {
            $errors['date'] = "Datum darf nicht in der Zukunft liegen";
            return false;
        } else {
            return true;
        }
    } catch (Exception $e) {
        $errors['date'] = "Datum ungültig";
        return false;
    }
}

function calculateBMI($height, $weight) {
    // Umrechnung von cm in m
    $heightInMeters = $height / 100;

    
    // BMI-Formel: gewicht / (größe * größe)
    $bmi = $weight / ($heightInMeters * $heightInMeters);
    
    echo "Der BMI beträgt: " . round($bmi, 1);
}


