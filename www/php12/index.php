<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <script type="text/javascript" src="js/index.js"></script>
    <title>BMI-Rechner</title>
</head>

<body>
    <div class="container">
        <h1 class="mt-5 mb-3">Body-Mass-Indedx-Rechner</h1>

        <?php

        require "lib/func.inc.php";

        $name = '';
        $height = '';
        $weight = '';
        $date = '';
        $info = '';
        //$bmi = '';

        if (isset($_POST['submit'])) {

            $name = isset($_POST['name']) ? $_POST['name'] : '';
            $height = isset($_POST['height']) ? $_POST['height'] : '';
            $weight = isset($_POST['weight']) ? $_POST['weight'] : '';
            $date = isset($_POST['date']) ? $_POST['date'] : '';

            if (validate($name, $date, $height, $weight)) {
                echo "<p class='alert alert-success'>Die eingegebenen Daten sind in Ordnung!</p>";
                calculateBMI($height, $weight);
            } else {
                echo "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
                foreach ($errors as $key => $value) {
                    echo "<li>" . $value . "</li>";
                }
                echo "</ul></div>";
            }
        }

        ?>

        <form id="form_weight" action="index.php" method="post">
            <div class="row">
                <div class="col-sm-8">
                    <div class="row">

                        <div class="col-sm-8 form-group">
                            <label for="name">Name*</label>
                            <input type="text" name="name" class="form-control <?= isset($errors['name']) ? 'is-invalid' : '' ?>" maxlength="20" value="<?= htmlspecialchars($name) ?>" required />
                        </div>

                        <div class="col-sm-4 form-group">
                            <label for="date">Messdatum*</label>
                            <input type="date" name="date" class="form-control <?= isset($errors['date']) ? 'is-invalid' : '' ?>" onchange="validatedate(this)" value="<?= htmlspecialchars($date) ?>" required />
                        </div>


                    </div>
                    <div class="row">

                        <div class="col-sm-6 form-group">
                            <label for="height">Größe (cm)*</label>
                            <input type="number" name="height" class="form-control <?= isset($errors['height']) ? 'is-invalid' : '' ?>" min="1" max="250" value="<?= htmlspecialchars($height) ?>" required />
                        </div>

                        <div class="col-sm-6 form-group">
                            <label for="weight">Gewicht (kg)*</label>
                            <input type="number" name="weight" class="form-control <?= isset($errors['weight']) ? 'is-invalid' : '' ?>" min="1" max="250" value="<?= htmlspecialchars($weight) ?>" required />
                        </div>

                    </div>

                </div>
                <div class="col">
                    <div>
                        <h3>Info zum BMI</h3>
                        <p>Unter 18.5 Untergewicht<br>
                            18.5 - 24.9 Normal<br>
                            25.0 - 29.9 Übergewicht<br>
                            30.0 und darüber Adipositas</p>
                    </div>
                </div>

            </div>
            <div class="row mt-3">

                <div class="col-sm-3 mb-3">
                    <input type="submit" name="submit" class="btn btn-primary w-100" value="Speichern" />
                </div>

                <div class="col-sm-3">
                    <a href="index.php" class="btn btn-secondary w-100">Löschen</a>
                </div>

            </div>


        </form>

    </div>

</body>

</html>