<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $vorname = htmlspecialchars($_POST['vorname']);
    $nachname = htmlspecialchars($_POST['nachname']);
    $email = htmlspecialchars($_POST['email']); // XSS protection
    $passwort = htmlspecialchars($_POST['passwort']);
    $browser = htmlspecialchars($_POST['browser']);
    $gefaellt = htmlspecialchars($_POST['gefaellt']);
    $verbesserungsvorschlaege = htmlspecialchars($_POST['verbesserungsvorschlaege']);
    $newsletter = htmlspecialchars($_POST['newsletter']);

    echo "Vorname: " . $vorname . "<br>";
    echo "Nachname: " . $nachname . "<br>";
    echo "Email: " . $email . "<br>";
    echo "Passwort: " . $passwort . "<br>";
    echo "Browser: " . $browser . "<br>";
    echo "Gefaellt mir: " . $gefaellt . "<br>";
    echo "Verbesserungsvorschlaege: " . $verbesserungsvorschlaege . "<br>";
    echo "Newsletter: " . $newsletter . "<br>";
}
?>
