# POS PHP Mitschrift

### Server

Apache server mit XAMMP oder DOCKER

### VS Code Extensions
PHP Intelephense

---

### Opening tag:
``` php
<?php
```


### Closing tag: (not needed if only php)
``` php
?> // only needed in html file
```

### Kommentare:
``` php
//
```

### Datentypen und Variablen
``` php
$string = "Ich bin ein String";
$i = 2; // Integer;
$d = 3.2 // decimal;
$n = null;
```

### Konstanten
``` php
const PI = 3.14159265359;
```

### Vergleichsoperatoren
| Operator    | Bedeutung                  | Beispiel     |
|-------------|----------------------------|--------------|
| `==`        | Gleichheit (wertbasiert)   | `$x == $y`   |
| `===`       | Identität (typ- und wertbasiert) | `$x === $y`  |
| `!=`        | Ungleichheit (wertbasiert) | `$x != $y`   |
| `!==`       | Ungleichheit (typ- und wertbasiert) | `$x !== $y`  |
| `>`         | Größer als                 | `$x > $y`    |
| `<`         | Kleiner als                | `$x < $y`    |
| `>=`        | Größer oder gleich         | `$x >= $y`   |
| `<=`        | Kleiner oder gleich        | `$x <= $y`   |
| `<>`        | Ungleichheit (wertbasiert) | `$x <> $y`   |

#### Beispiel:

``` php
<?php
var_dump(0 == "a");
var_dump("1" == "01");
var_dump("10" == "1e1");
var_dump(100 == "1e2");

switch ("a") {
case 0:
    echo "0";
    break;
case "a":
    echo "a";
    break;
}
?>
```

### Verküpfungen
| Operator | Bedeutung             | Beispiel                |
|----------|-----------------------|-------------------------|
| `&&`     | Logisches UND         | `$x && $y`              |
| `\|\|`     | Logisches ODER         | `$x \|\| $y`              |
| `!`      | Logisches NICHT        | `!$x`                   |
| `and`    | Logisches UND (alternativ) | `$x and $y`          |
| `or`     | Logisches ODER (alternativ) | `$x or $y`            |
| `xor`    | Logisches EXKLUSIV-ODER | `$x xor $y`            |

### If-Anweisung
``` php
if($x==$y){
    echo "x ist y";
}else{
    echo "x ist nicht y";
}
```


// auch while, do ...

### Schleifen
``` php
for ($i = 0; $i < 5; $i++){
    echo $i . "<br>"; // concat with "."
}
```

### Arrays
``` php
$array = array(1,2,3,4,5);
$array1 = [6,7,8,9];
```

### Assoziative Arrays
``` php
$age = array("Patrick" => 35, "Tom" => 30);
```

### foreach
``` php
foreach ($array as $wert){
    echo $wert . ", ";
}

foreach ($age as $key => $value){
    echo $key . " ist " . $value . "Jahre alt.<br>";
}
```

### Funktionen
``` php
function greet($name = "Patrick Mairhofer"){
    //echo "Hallo " . $name;
    return "Hallo " . $name;
}
echo greet();

// call by value       =>   ohne &
// call by reference   =>   mit &
```

---


# PHP Grundlagen (Kapitel 15)

Der Begriff PHP ist ein sogenanntes rekursives Akronym und steht für "Hypertext Preprocessor".

PHP wird meist dazu verwendet, dynamische Webseiten oder -anwendungen zu programmieren.

Seit Version 8.1 kann PHP sogar Fiber-Support. Dies erlaubt die Entwicklung von asynchron laufenden Programmteilen.

## Lokale Installation von PHP und Webserver

### Mit MAMP (für macOS):

1. **MAMP herunterladen und installieren**:
   - [MAMP für macOS](https://www.mamp.info/)

2. **Starte MAMP**:
   - Starte die installierte Software. Sie enthält Apache (Webserver), MySQL (Datenbank) und PHP.

3. **Webserver- und PHP-Einstellungen**:
   - Konfiguriere den Webserver und PHP über die Benutzeroberfläche von MAMP.
   - Passe den Port, den Document Root und andere Einstellungen nach Bedarf an.

4. **Testen**:
   - Erstelle eine PHP-Datei (z.B., `index.php`) im Document Root-Verzeichnis.
   - Beispielinhalt: `<?php phpinfo(); ?>`

5. **Webserver starten**:
   - Starte den Webserver über die Software (MAMP).

6. **Im Webbrowser öffnen**:
   - Öffne deinen Webbrowser und gehe zur URL, um deine PHP-Datei aufzurufen (z.B., http://localhost/index.php).

### Mit XAMPP (für macOS, Windows, Linux):

1. **XAMPP herunterladen und installieren**:
   - [XAMPP für macOS, Windows, Linux](https://www.apachefriends.org/index.html)

2. **Starte XAMPP**:
   - Starte die installierte Software. Sie enthält Apache (Webserver), MySQL (Datenbank) und PHP.

3. **Webserver- und PHP-Einstellungen**:
   - Konfiguriere den Webserver und PHP über die Benutzeroberfläche von XAMPP.
   - Passe den Port, den Document Root und andere Einstellungen nach Bedarf an.

4. **Testen**:
   - Erstelle eine PHP-Datei (z.B., `index.php`) im Document Root-Verzeichnis.
   - Beispielinhalt: `<?php phpinfo(); ?>`

5. **Webserver starten**:
   - Starte den Webserver über die Software (XAMPP).

6. **Im Webbrowser öffnen**:
   - Öffne deinen Webbrowser und gehe zur URL, um deine PHP-Datei aufzurufen (z.B., http://localhost/index.php).

### Mit Docker:

1. **Docker installieren**:
   - [Docker herunterladen und installieren](https://www.docker.com/get-started)

2. **PHP-Docker-Container ausführen**:
   - Verwende `docker-compose up` oder `docker run`, um einen PHP-Container zu starten.
   - Konfiguriere die Container-Umgebung nach Bedarf.

3. **Testen**:
   - Erstelle eine PHP-Datei (z.B., `index.php`) im Arbeitsverzeichnis des Containers.
   - Beispielinhalt: `<?php phpinfo(); ?>`

4. **Container starten**:
   - Starte den Docker-Container.

5. **Im Webbrowser öffnen**:
   - Öffne deinen Webbrowser und gehe zur URL, um deine PHP-Datei aufzurufen (z.B., http://localhost/index.php).


## Variablen und Datentypen

### Variablen

| Datentyp         | Beschreibung                               | Beispiel               |
|------------------|-------------------------------------------|-------------------------|
| Integer (int)    | Ganzzahlen                                 | `$zahl = 42;`           |
| Float (float)    | Gleitkommazahlen                           | `$pi = 3.14159;`        |
| String (string)  | Zeichenketten                              | `$name = "John";`       |
| Boolean (bool)   | Wahrheitswerte (true/false)                | `$ist_wahr = true;`     |
| Array (array)    | Listen von Werten                          | `$farben = ["rot", "grün", "blau"];` |
| Associative Array | Assoziative Arrays (Schlüssel-Wert-Paare) | `$person = ["name" => "John", "alter" => 30];` |
| Null (null)      | Kein Wert (Abwesenheit von Wert)           | `$nichts = null;`       |

### Datentypen

| Datentyp               | Beschreibung                                           |
|------------------------|-------------------------------------------------------|
| **Skalare Typen**      |                                                       |
| Integer (int)          | Ganzzahlen.                                           |
| Float (float)          | Gleitkommazahlen.                                     |
| String (string)        | Zeichenketten.                                        |
| Boolean (bool)         | Wahrheitswerte (true/false).                          |
| **Zusammengesetzte Typen** |                                                   |
| Array (array)          | Listen von Werten.                                    |
| Callable               | Callback-Funktionen                                   |
| Object (object)        | Instanzen von Klassen.                                |
| Iterable                | Akzeptiert Array oder Objekt das Transversible-Interface implementiert |
| **Spezielle Typen**    |                                                       |
| Null (null)            | Kein Wert (Abwesenheit von Wert).                     |
| Resource               | Ressourcen wie Datei-Handles oder Datenbankverbindungen. |

> In PHP wir der Variablentyp durch den konkreten Wert bestimmt!

#### Beispiel:

``` php
<? php
$var = '1';             // $var ist ein String
$var = $var * 1;        // $var ist jetzt ein Integer
$var = (string) $var;  // $var ist wieder ein String
```

Um einen bestimmten Typ zu casten verwendet man: `settype()`

### Referenzen

Mit dem **&-Operator** kann man die Referenz einer Variablen verändern.

#### Beispiel - Zuweisung einer Referenz:

``` php
<? php
$lastName = 'Mustermann';
$birthName = &$lastName;  // enthält jetzt ebenfalls den Wert 'Mustermann'
$birthName = 'Musterfrau';  // $birthName wird verändert. Dadurch auch $lastName
echo $lastName;  // Ausgabe: 'Musterfrau'#
```

### Vordefinierte Variablen

| Vordefinierte Variable   | Beschreibung                                     |
|--------------------------|-------------------------------------------------|
| `$_GET`                  | Enthält Daten aus einem HTTP GET-Request.      |
| `$_POST`                 | Enthält Daten aus einem HTTP POST-Request.     |
| `$_REQUEST`              | Enthält Daten aus GET-, POST- und COOKIE-Requests. |
| `$_SESSION`              | Enthält Informationen über die aktuelle Sitzung. |
| `$_COOKIE`               | Enthält Cookies, die an den Server gesendet wurden. |
| `$_FILES`                | Enthält Informationen über hochgeladene Dateien. |
| `$_SERVER`               | Enthält Informationen über den Server und die Umgebung. |
| `$_ENV`                  | Enthält Umgebungsvariablen des Servers.         |
| `$GLOBALS`               | Enthält alle globalen Variablen.                |
| `$_GLOBALS`              | Frühere Version von `$GLOBALS`.                  |

> NOTE: `var_dump($_GET)` sorgt für eine ansehnliche Ausgabe im Browser.

### Konstanten

Definiert werden Konstanten durch die Funktion `define()`. Der Aufruf erfolgt ohne das $-Zeichen.

Best practice ist es die **Konstanten in Großbuchstaben** zu schreiben.

#### Beispiel - Definition von Konstanten:

``` php
<?php
define('MINUTE_IN_SECONDS', 60);
define('HOUR_IN_SECONDS', 60 * MINUTE_IN_SECONDS);
define('DAY_IN_SECONDS', 24 * HOUR_IN_SECONDS);
```

### Operatoren verwenden

PHP bietet eine Vielzahl von Operatoren, die in PHP-Anwendungen verwendet werden können, um verschiedene Aufgaben auszuführen. Hier sind einige der häufig verwendeten PHP-Operatoren:

- Arithmetische Operatoren: Diese Operatoren werden verwendet, um mathematische Operationen durchzuführen, z. B. Addition (+), Subtraktion (-), Multiplikation (*), Division (/) und Modulo (%).

- Vergleichsoperatoren: Mit Vergleichsoperatoren können Sie Werte vergleichen, z. B. Gleichheit (== oder ===), Ungleichheit (!= oder !==), Größer als (>), Kleiner als (<), usw.

- Logische Operatoren: Diese Operatoren werden verwendet, um logische Ausdrücke zu verknüpfen oder zu verneinen, z. B. && (und), || (oder) und ! (nicht).

- Zuweisungsoperatoren: Diese Operatoren werden verwendet, um Werte Variablen zuzuweisen, z. B. = (Zuweisung), += (Zuweisung mit Addition), -= (Zuweisung mit Subtraktion) usw.

- Inkrement- und Dekrementoperatoren: Diese Operatoren werden verwendet, um den Wert einer Variable um 1 zu erhöhen (++) oder zu verringern (--).

- Zeichenkettenoperatoren: Der Punkt (.) wird verwendet, um Zeichenketten zu konkatenieren (zusammenzuführen).

- Bedingungsoperator (Ternäroperator): Der bedingte Operator (? :) ermöglicht es, abhängig von einer Bedingung zwei verschiedene Werte zurückzugeben.

- Array-Operatoren: Die +-Operator und .-Operator werden verwendet, um Arrays zusammenzuführen und Schlüssel/Werte in Arrays zu verketten.
  
- Bit-Operatoren in PHP ermöglichen die Manipulation von Bitmustern in ganzen Zahlen. Hier sind einige der häufig verwendeten Bit-Operatoren:
```
& (AND): Führt eine Bitweise-UND-Operation durch.
| (OR): Führt eine Bitweise-ODER-Operation durch.
^ (XOR): Führt eine Bitweise-EXKLUSIVE-ODER-Operation durch.
~ (NOT): Führt eine Bitweise-NICHT-Operation durch (invertiert Bits).
<< (Verschiebung nach links): Verschiebt die Bits um eine bestimmte Anzahl von Stellen nach links.
>> (Verschiebung nach rechts): Verschiebt die Bits um eine bestimmte Anzahl von Stellen nach rechts.
```

- Ternärer Operator (Bedingungsoperator) in PHP**:

    Der ternäre Operator `? :` ermöglicht es, eine Bedingung in einer kompakten Form zu überprüfen und basierend auf der Bedingung einen von zwei Werten zurückzugeben.

Hier ist ein Beispiel für die Verwendung des ternären Operators:

```php
$alter = 18;
$status = ($alter >= 18) ? 'Volljährig' : 'Minderjährig';

echo $status; // Ausgabe: Volljährig (wenn $alter >= 18)
```

Hier ist ein Beispiel, das die Verwendung des Vergleichsoperators == in PHP zeigt:

``` php
$zahl1 = 5;
$zahl2 = 10;

if ($zahl1 == $zahl2) {
    echo "Die Zahlen sind gleich.";
} else {
    echo "Die Zahlen sind nicht gleich.";
}
```

- Der Raumschiffvergleichsoperator
  
``` php
<?php
$a = 5;
$b = 6;
echo $a <=> $b;
// Ausgabe: -1 => weil a kleiner b ist
// Wäre a größer b => Ausgabe: 1
// Bei gleichen Werten => Ausgabe 0
```

## Kontrollstrukturen verwenden

### Bedingte Anweisungen

Bedingte Anweisungen sind ein grundlegendes Konzept in der Programmierung, das es ermöglicht, Code basierend auf bestimmten Bedingungen auszuführen oder zu überspringen. In PHP können Sie bedingte Anweisungen mit den Schlüsselwörtern if, else if und else erstellen.
if Anweisung

#### if Anweisung

Beispiel:

``` php
$zahl = 10;

if ($zahl > 5) {
    echo "Die Zahl ist größer als 5.";
}
```

#### else Anweisung

Die else-Anweisung wird verwendet, um Code auszuführen, wenn die Bedingung in der if-Anweisung falsch ist.

Beispiel:
``` php
$zahl = 3;

if ($zahl > 5) {
    echo "Die Zahl ist größer als 5.";
} else {
    echo "Die Zahl ist nicht größer als 5.";
}
```

#### else if Anweisung

Die else if-Anweisung ermöglicht es, mehrere Bedingungen nacheinander zu überprüfen. Der Code in der ersten wahren Bedingung wird ausgeführt, und der Rest wird übersprungen.

Beispiel:

php

$zahl = 4;

if ($zahl > 5) {
    echo "Die Zahl ist größer als 5.";
} else if ($zahl == 5) {
    echo "Die Zahl ist gleich 5.";
} else {
    echo "Die Zahl ist kleiner als 5.";
}

Bedingte Anweisungen sind äußerst nützlich, um in Ihren PHP-Programmen Entscheidungen zu treffen und unterschiedlichen Code je nach den gegebenen Bedingungen auszuführen.
User
kannst du mir das alles bitte so schreiben, dass ich alles auf einmal in ein markkdown file kopieren kann
ChatGPT

Natürlich! Hier ist der Markdown-Code, den du in eine Markdown-Datei kopieren kannst:

markdown

# Bedingte Anweisungen in PHP

Bedingte Anweisungen sind ein grundlegendes Konzept in der Programmierung, das es ermöglicht, Code basierend auf bestimmten Bedingungen auszuführen oder zu überspringen. In PHP können Sie bedingte Anweisungen mit den Schlüsselwörtern `if`, `else if` und `else` erstellen.

#### if Anweisung
Die `if`-Anweisung führt Code aus, wenn eine Bedingung wahr ist. Wenn die Bedingung falsch ist, wird der Code übersprungen.

Beispiel:
```php
$zahl = 10;

if ($zahl > 5) {
    echo "Die Zahl ist größer als 5.";
}
```

#### else Anweisung

Die else-Anweisung wird verwendet, um Code auszuführen, wenn die Bedingung in der if-Anweisung falsch ist.

Beispiel:

``` php
$zahl = 3;

if ($zahl > 5) {
    echo "Die Zahl ist größer als 5.";
} else {
    echo "Die Zahl ist nicht größer als 5.";
}
```

#### else if Anweisung

Die else if-Anweisung ermöglicht es, mehrere Bedingungen nacheinander zu überprüfen. Der Code in der ersten wahren Bedingung wird ausgeführt, und der Rest wird übersprungen.

Beispiel:

``` php
$zahl = 4;

if ($zahl > 5) {
    echo "Die Zahl ist größer als 5.";
} else if ($zahl == 5) {
    echo "Die Zahl ist gleich 5.";
} else {
    echo "Die Zahl ist kleiner als 5.";
}
```

#### switch Anweisung

Die switch Anweisung ermöglicht es, zwischen verschiedenen möglichen Werten oder Fällen zu wählen und Code basierend auf einem übereinstimmenden Fall auszuführen. 

Beispiel

```php
$tag = "Montag";

switch ($tag) {
    case "Montag":
        echo "Heute ist Montag.";
        break;
    case "Dienstag":
        echo "Heute ist Dienstag.";
        break;
    case "Mittwoch":
        echo "Heute ist Mittwoch.";
        break;
    default:
        echo "Heute ist ein anderer Tag.";
}
```

## Schleifen

In PHP gibt es verschiedene Arten von Schleifen, die verwendet werden können, um repetitive Aufgaben zu automatisieren. In diesem Tutorial werden wir die wichtigsten Schleifentypen kennenlernen: for, while, do-while und foreach.

### For-Schleife

``` php
for ($i = 1; $i <= 5; $i++) {
    echo "Iteration $i<br>";
}
```

### While-Schleife

``` php
$num = 1;
while ($num <= 5) {
    echo "Aktueller Wert: $num<br>";
    $num++;
}
```

### Do-While-Schleife

```php
$num = 1;
do {
    echo "Aktueller Wert: $num<br>";
    $num++;
} while ($num <= 5);
```

### Foreach-Schleife

``` php
$fruits = array("Apfel", "Banane", "Kirsche");
foreach ($fruits as $fruit) {
    echo "Frucht: $fruit<br>";
}
```

## Funktionen

### Funktionen definieren

``` php
<?php
// Funktion definieren
function greet($name) {
    echo "Hallo, $name!";
}

// Funktion aufrufen
greet("Max");
?>
```

### Funktionsparameter

``` php
<?php
// Funktion definieren, die zwei Zahlen addiert
function add($a, $b) {
    $sum = $a + $b;
    return $sum;
}

// Funktion aufrufen und das Ergebnis speichern
$result = add(5, 3);

// Das Ergebnis ausgeben
echo "Die Summe von 5 und 3 ist: " . $result;
?>
```

#### Variable Anzahl an Parametern

``` php
<?php
// Funktion definieren, die eine beliebige Anzahl von Zahlen addiert
function addNumbers(...$numbers) {
    $sum = 0;
    foreach ($numbers as $num) {
        $sum += $num;
    }
    return $sum;
}

// Funktion aufrufen und das Ergebnis speichern
$result1 = addNumbers(5, 3, 7);
$result2 = addNumbers(10, 20, 30, 40);

// Die Ergebnisse ausgeben
echo "Die Summe von 5, 3 und 7 ist: " . $result1 . "<br>";
echo "Die Summe von 10, 20, 30 und 40 ist: " . $result2;
?>
```

### Funktion mit Typendeklaration

``` php
<?php
// Funktion definieren, die eine beliebige Anzahl von Zahlen addiert
function addNumbers(...$numbers) {
    $sum = 0;
    foreach ($numbers as $num) {
        $sum += $num;
    }
    return $sum;
}

// Funktion aufrufen und das Ergebnis speichern
$result1 = addNumbers(5, 3, 7);
$result2 = addNumbers(10, 20, 30, 40);

// Die Ergebnisse ausgeben
echo "Die Summe von 5, 3 und 7 ist: " . $result1 . "<br>";
echo "Die Summe von 10, 20, 30 und 40 ist: " . $result2;
?>
```

### Anonyme Funktionen

``` php
<?php
// Eine anonyme Funktion definieren und in einer Variablen speichern
$addNumbers = function($a, $b) {
    return $a + $b;
};

// Die anonyme Funktion aufrufen und das Ergebnis speichern
$result = $addNumbers(5, 3);

// Das Ergebnis ausgeben
echo "Die Summe von 5 und 3 ist: " . $result;
?>
```

Variablenfunktionen deklarieren

``` php
<?php
// Funktion 1 definieren
function greetInEnglish($name) {
    echo "Hello, $name!";
}

// Funktion 2 definieren
function greetInGerman($name) {
    echo "Hallo, $name!";
}

// Eine Variable mit dem Namen der Funktion festlegen
$language = "greetInEnglish";

// Die Variable als Funktion aufrufen
$name = "John";
$language($name); // Aufruf von greetInEnglish("John");

// Die Variable auf die andere Funktion umstellen
$language = "greetInGerman";
$name = "Max";
$language($name); // Aufruf von greetInGerman("Max");
?>
```

### Pfeilfunktionen

``` php
<?php
§numbersSquared = array_map(
    fn($number) => pow($number, 2),
    $numbers
);
```

### Fehler

``` php
<?php
...
try{
    printNumbers(100,0);
    } catch (Error $e){
        echo "Fehler" . $e->getMessage();
    }
```

## Klassen und Objekte verwenden (Seite 22)

Klassen sind in PHP ein grundlegendes Konzept der objektorientierten Programmierung (OOP). Sie ermöglichen die Definition von Datentypen, die sowohl Daten als auch Funktionen (Methoden) enthalten können. Hier ist eine kurze Einführung zur Verwendung von Klassen in PHP:

1. **Klassen definieren:**
   Eine Klasse wird mit dem Schlüsselwort `class` erstellt, gefolgt vom Klassennamen. Der Klassennamen sollte in PascalCase (Wörter werden groß geschrieben, ohne Leerzeichen) geschrieben werden.

   ```php
   class Person {
       // Klassenmitglieder (Variablen und Methoden) werden hier definiert
   }
   ```

2. **Klassenmitglieder:**
   Innerhalb einer Klasse können Sie Klassenmitglieder wie Eigenschaften (Variablen) und Methoden (Funktionen) definieren.

   ```php
   class Person {
       public $name; // Eigenschaft
       
       public function sayHello() { // Methode
           echo "Hallo, ich bin " . $this->name;
       }
   }
   ```

3. **Objekte erstellen:**
   Sie erstellen ein Objekt einer Klasse, indem Sie das `new`-Schlüsselwort verwenden.

   ```php
   $person1 = new Person();
   $person2 = new Person();
   ```

4. **Eigenschaften zugreifen:**
   Sie können auf Eigenschaften eines Objekts zugreifen, indem Sie den Pfeiloperator (`->`) verwenden.

   ```php
   $person1->name = "Max";
   ```

5. **Methoden aufrufen:**
   Sie können Methoden eines Objekts aufrufen, indem Sie ebenfalls den Pfeiloperator verwenden.

   ```php
   $person1->sayHello(); // Ausgabe: Hallo, ich bin Max
   ```

6. **Konstruktor und Destruktor:**
   Sie können spezielle Methoden namens `__construct` (Konstruktor) und `__destruct` (Destruktor) in einer Klasse definieren, um Initialisierungs- und Bereinigungsarbeiten durchzuführen.

   ```php
   class Person {
       public $name;
       
       public function __construct($name) {
           $this->name = $name;
       }
       
       public function sayHello() {
           echo "Hallo, ich bin " . $this->name;
       }
   }
   
   $person = new Person("Max");
   ```

Klassen sind ein leistungsstarkes Konzept in PHP, das Ihnen ermöglicht, Code zu organisieren, zu kapseln und wiederzuverwenden. Sie unterstützen die Prinzipien der OOP wie Vererbung, Abstraktion, Kapselung und Polymorphismus, was die Entwicklung von komplexen Anwendungen erleichtert.


### Sichtbarkeit

1. **Öffentlich (public):**
   - Eigenschaften und Methoden, die als `public` deklariert sind, sind von überall zugänglich, sowohl innerhalb als auch außerhalb der Klasse.
   - Dies ist die am häufigsten verwendete Sichtbarkeit und wird oft für Mitglieder verwendet, die von anderen Teilen des Codes verwendet werden müssen.

   ```php
   class MyClass {
       public $publicProperty;
       
       public function publicMethod() {
           // ...
       }
   }
   ```

2. **Geschützt (protected):**
   - Eigenschaften und Methoden, die als `protected` deklariert sind, können nur innerhalb der Klasse selbst und von abgeleiteten Klassen (in der Vererbungshierarchie) zugegriffen werden.
   - Dies bietet eine begrenzte Sichtbarkeit, die für Mitglieder verwendet wird, die speziell für die Verwendung in Unterklasse entwickelt wurden.

   ```php
   class MyBaseClass {
       protected $protectedProperty;
       
       protected function protectedMethod() {
           // ...
       }
   }
   ```

3. **Privat (private):**
   - Eigenschaften und Methoden, die als `private` deklariert sind, sind nur innerhalb der Klasse selbst zugänglich.
   - Diese Sichtbarkeit bietet maximale Einschränkung und wird oft verwendet, um Mitglieder zu definieren, die nur innerhalb der Klasse verwendet werden sollen, um Dateninkapselung zu gewährleisten.

   ```php
   class MyClass {
       private $privateProperty;
       
       private function privateMethod() {
           // ...
       }
   }
   ```

## Vererbung

Vererbung ist ein wichtiges Konzept in der objektorientierten Programmierung (OOP) und ermöglicht es in PHP, Klassen zu erstellen, die Eigenschaften und Methoden von anderen Klassen erben. Dies erleichtert die Wiederverwendung von Code und die Organisation von Klassen in einer Hierarchie. Hier sind die Grundlagen der Vererbung in PHP:

1. **Basisklasse (Elternklasse):**
   - Die Basisklasse ist die ursprüngliche Klasse, von der andere Klassen erben.
   - Sie enthält allgemeine Eigenschaften und Methoden, die von abgeleiteten Klassen gemeinsam genutzt werden können.

   ```php
   class Animal {
       public $name;
       
       public function speak() {
           echo "Das Tier spricht.";
       }
   }
   ```

2. **Abgeleitete Klasse (Kindklasse):**
   - Die abgeleitete Klasse erbt Eigenschaften und Methoden von der Basisklasse.
   - Sie kann auch zusätzliche Eigenschaften und Methoden hinzufügen oder die geerbten Methoden überschreiben.

   ```php
   class Dog extends Animal {
       public function speak() {
           echo "Der Hund bellt.";
       }
       
       public function wagTail() {
           echo "Der Hund wedelt mit dem Schwanz.";
       }
   }
   ```

3. **Vererbung nutzen:**
   - Um eine Klasse zu erben, verwenden Sie das Schlüsselwort `extends`, gefolgt vom Namen der Basisklasse.

   ```php
   $dog = new Dog();
   $dog->name = "Rex";
   $dog->speak();     // Ausgabe: Der Hund bellt.
   $dog->wagTail();   // Ausgabe: Der Hund wedelt mit dem Schwanz.
   ```

4. **Zugriff auf geerbte Mitglieder:**
   - In der abgeleiteten Klasse können Sie auf geerbte Mitglieder (Eigenschaften und Methoden) mithilfe des Pfeiloperators (`->`) zugreifen.

   ```php
   $dog->name = "Rex";
   $dog->speak();     // Ausgabe: Der Hund bellt.
   ```

5. **Überschreiben von Methoden:**
   - In abgeleiteten Klassen können Sie geerbte Methoden überschreiben, indem Sie eine neue Implementierung bereitstellen.

   ```php
   class Cat extends Animal {
       public function speak() {
           echo "Die Katze miaut.";
       }
   }
   ```

6. **Konstruktor in abgeleiteten Klassen:**
   - Abgeleitete Klassen können einen eigenen Konstruktor haben. In diesem Fall müssen Sie die Elternklasse explizit aufrufen, um deren Konstruktor auszuführen.

   ```php
   class Dog extends Animal {
       public function __construct($name) {
           parent::__construct($name);
       }
   }
   ```

## Klassenabstraktion

Klassenabstraktion ist ein weiteres wichtiges Konzept der objektorientierten Programmierung (OOP) in PHP. Die Abstraktion ermöglicht es, komplexe Systeme zu vereinfachen, indem Sie nur die wesentlichen Merkmale und Funktionen eines Objekts oder einer Klasse erfassen und irrelevante Details ausblenden. Hier sind die Grundlagen der Klassenabstraktion in PHP:

1. **Abstrakte Klassen:**
   - Eine abstrakte Klasse ist eine Klasse, die nicht direkt instanziiert werden kann und dazu dient, allgemeine Struktur und Methoden für abgeleitete Klassen bereitzustellen.
   - Abstrakte Klassen werden mit dem Schlüsselwort `abstract` deklariert.

   ```php
   abstract class Shape {
       abstract public function calculateArea();
   }
   ```

2. **Abstrakte Methoden:**
   - Abstrakte Klassen können abstrakte Methoden enthalten, die in abgeleiteten Klassen implementiert werden müssen.
   - Abstrakte Methoden werden mit dem Schlüsselwort `abstract` deklariert und enthalten keine Implementierung.

   ```php
   abstract class Shape {
       abstract public function calculateArea();
   }
   ```

3. **Abgeleitete Klassen:**
   - Abgeleitete Klassen erben von abstrakten Klassen und müssen alle abstrakten Methoden implementieren.
   - Sie können auch zusätzliche Methoden und Eigenschaften hinzufügen.

   ```php
   class Circle extends Shape {
       private $radius;
       
       public function __construct($radius) {
           $this->radius = $radius;
       }
       
       public function calculateArea() {
           return 3.14 * $this->radius * $this->radius;
       }
   }
   ```

4. **Verwendung der Abstraktion:**
   - Die Abstraktion erlaubt es, Objekte zu erstellen und Methoden aufzurufen, ohne sich um die internen Details der abstrakten Klasse zu kümmern.

   ```php
   $circle = new Circle(5);
   $area = $circle->calculateArea();
   ```

## Dynamische Webseiten mit PHP

Wurde auf gitlab geladen und funktioniert soweit.

### Formular empfangen

Die form.php wird zwar an den PHP-Interepreter übergeben, es enthält jedoch noch keinen PHP-Code. Nach dem Absenden führt der Braowser die selbe  Datei nochmal aus. Erst dann werden die Daten aus dem Formular mit der POST-Methode mit Hilfe der vordefinierten $_POST Variablen übertragen.

### Formular prüfen

Weil wir nicht wissen ob das Formular bösartige Daten  enthält müssen wir dies überprüfen. 

3 Typen der Neutralisierung:

- Sanitisation (Säuberung) - Dateien so manipulieren, dass sie die Bedingungen der Validierung erfüllt.
- Validation (Validierung) - Beispielsweise die Vorgabe, dass in einer Email ein @ Zeichen sein muss.
- Escaping - HTML oder Skript-Tags entfernen, um zu verhindern, dass die Daten keinen Code ausführen können.
  
### Formulardaten säubern

PHP bietet hier eine Funktion `filter_input_arry()` bei der die INPUT_POST bestimmt, dass die Daten durch die POST-Methode übermittelt wurde. Dann nimmt es einen Array entgegen, der Säuberungsfilter wird angewendet.

``` php
<?php
if( ! empty($_POST)){
    $formFields = [
        'firstname' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'lastname' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'email' => FILTER_SANITIZE_EMAIL,
        'password' => FILTER_UNSAFE_RAW,
        'browser' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'feedback' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'improvements' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'newletter' => FILTER_SANITIZE_FULL_SPECIAL_CHARS
    ];
    $formData = filter_input_array(
        INPUT_POST,
        $formfields
    );
}
```

### Formulardaten validieren

Validierung:

``` php
<?php
if( ! empty($_POST)){
    $formFields = [
        'firstname' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'lastname' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'email' => FILTER_SANITIZE_EMAIL | FILTER_VALIDATE_EMAIL,
        'password' => [
            'filter' => FILTER_CALLBACK,
            'flags' => FILTER_REQUIRE_SCALAR,
            'options' => 'hashpasswrod'
        ]
        'browser' => [
            'filter' => FILTER_CALLBACK,
            'flags' => FILTER_REQUIRE_SCALAR,
            'options' => 'enumbrowsers'
        ]
        'feedback' => FILTER_VALIDATE_BOOLEAN,
        'improvements' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'newletter' => FILTER_VALIDATE_BOOLEAN
    ];
    $formData = filter_input_array(
        INPUT_POST,
        $formfields
    );
}
```

Benutzerdefinierte Callback-Funktionen:

``` php
<?php
define (
    'PASSWORD_SALT',
    '1234'
);
function enumBrowsers ($value): ?string {
    $possibleValues = [
        'chrome',
        'edge',
        'opera',
        'safari',
        'firefox'
    ];
    foreach ($possibleValues as $possibleVaalue){
        if($possibleValue === $value){
            return $value;
        }
    }
    return null;
}

function hashPassword ($password): string {
    return ! empty($password) ? crypt($password, PASSWORD_SALT):'';
}
```

### Daten verarbeiten

Formulardaten lassen sich auf unterschiedliche Weise verarbeiten.

Versenden einer einfachen Mail:

```php
<?php
...
if (count($errorFields)) > 0 ) {
    . implode(', ', $errorFields)
    . '!</strong></p>';
} else {
    $sendData = $formData;
    array_walk($sendData, fn (&$val, $key) =>$val = $key . ': ' . $val);
    $message = implode("\r\n", $sendData);
    $mailSent = mail(
        'info@mail.at',
        'Mail vom Anmeldeformular',
        wordwrap($message, 70, "\r\n")
    );
    if (! $mailSent) {
        echo '<p><strong>Leider konnte die Email'
        . 'nicht versandt werden.</strong></p>';
    }
}
```

Die Funktion `mail()` ist nicht dazu geeignet viele Mails zu senden, da mit jeder Mail ein SMTP-Socket geöffnet und geschlossen wird oder der Dienst vom Host gesperrt ist. Durch die Verwendung von einem Framework wie PHPMailer können wir eine SMTP-Verbindung herstellen.


## Zusammenfassung

- PHP wird immer Serverseitig ausgeführt.
- PHP hat 10 primitive und einen besonderen Datentyp `object`.
- Es gibt vordefinierte Variablen wie `$_POST`.
- Einmal erstellte Variablen können nicht mehr verändert werden.
- Es gibt eine Vielzahl von Operatoren.
- Der Zuweisungsoperator weist einen Wert zu.
- Arithmetische Operatoren dienen zum Rechnen.
- Kombinierte Operatoren.
- Bit-Operatoren erlauben die Prüfung und Manipulation von Bits.
- Vergleichsoperatoren.
- Der Operator zur Programmausführung kann Komandozeilenprogramme starten.
- Logische Operatoren arbeiten nur mit boolschen Werten.
- Array-Operatoren vergleichen oder manipulieren Arrays.
- `instanceof` ist ein Typ-Operator, der auf eine bestimmte Klasse prüfen kann.
- Längere if-Anweisungen lassen sich mit dem ternären Operator ersetzen und das Ergebniss in eine Variable schreiben.
- Anweisungen und Schleifen erlauben das Steuern des Codeflusses.
- Funktionen definieren wiederverwendbare Codeteile. Optional mit Parametern.
- Closures sind anonyme Funktionen, die keinen Namen haben.
- Durch Pfeilfunktionen lassen sich Funktionen verkürzt darstellen.
- Fehler können mit einem `try-catch`-Block abgefangen werden.
- Es ist möglich eigene Klassen zu erstellen.
- Abstrakte Klassen dienen als Muster für Entwickler.
- Durch Überladung lassen sich dynamisch Eigenschaften und Methoden erstellen.


## Lernaufgabe 1: Notenerfassung

### Expertenvideo zur Lernaufgabe 1.1 - Teil 1 Problemstellung

Entwickle einen funktionsfähigen Prototypen nach der Vorlage des Mockups.

#### Prototyp sollte folgendes enthalten:
- Formular mit Beschriftung
- Löschfunktion
- Client- und Serverseitige Validierung
  - Clientseitig vor dem senden
  - Serverseitig nach dem Übermitteln
- Ausgabe der Daten nach Übermittlung zur Kontrolle
- Fehlermeldungen bei Validierungsproblemen
- Responsive Design
- Verwendung von Bootstrap

### Expertenvideo zur Lernaufgabe 1.1 - Teil 2 Lösungsansatz

1. Formular erstellen
    Das HTML-Formular wird direkt in die Startseite index.php integriert.
    Für die Übertragung der Daten wird die HTTP Post Methode verwendet.

2. Eingabefelder definieren
    Jedes Eingabefeld ist ein eigenes HTML-Input-Element mit entsprechendem Typen.
    Jedes Eingabefeld braucht einen eindeutigen Parameter `name`.
    Zur Übermittlung wird ein Submit-Button benötigt.
    Zum leeren wird ein Reset-Button benötigt.
    Reset-Button mit Javascript oder Formular-Reset.

3. Clientseitige Validierung
    Über HTML und Javascript
    Vorteil ist das direkte Feedback für den User.
    Name, Prüfungsdatum, Fach und Note werden als required markiert.
    Datumsüberprüfung, ob das Datum in der Zukunft ist.


4. Serverseitige Validierung
    MUSS IMMER durchgeführt werden.
    Nur so kann sicher gestellt werden, dass keine falschen Daten verarbeitet oder gespeichert werden.
    Über `$_POST` (assioziatives Array) kann auf die Formulardaten zugegriffen werden.
    Mit PHP-Funktionen werden die Input-Felder validiert.
    Eine Gesamtvalidierungsfunktion welche einen boolschen Wert zurück liefert, um zu sehen ob die ganze Validierung TRUE oder FALSE ergeben hat.
    Bei Auftreten von Fehlern werden die individuellen Fehlermeldungen dem User angezeigt.

5. Datenverarbeitung
    Speicherung der Werte

6. Ausgabe der Ergebnisse
    Fehler in rot anzeigen
    Keine Fehler in Grün anzeigen.

### Expertenvideo zur Lernaufgabe 1.1 - Teil 3 Implementierung

Zur Erstellung verwenden wir die Mockup-Vorlage und Bootstrap.

1. Anlegen einen neuen Projekts (php11).
2. Das Starter-Template von Bootstrap verwenden.
3. Neue Ordner für JS, CSS und LIB anlegen.
4. Anlegen von index.php (Haupt-File) und Starter-Template (html) hinein kopieren.
5. JQUERY löschen (brauchen wir noch nicht). CSS-Files nicht von externen Resourcen sondern direkt aus unserem CSS-Folder.
6. Gesamtes Bootstrap opder in unserem Fall die css-source aus dem Browser kopieren und in unserem CSS-Folder speichern und den Pfad in der index.php anpassen.
7. Sollte hier ein Fehler auftreten, kann man diesen in der Netzwerkanalyse des Browsers sehen.
8. Umsetzung der Vorlage:
  <div class="container"><h4 class="mt-5 mb-3">Notenerfassung</h1></div>

```php
<form id="form_grade" action="index.php" method="post">
</form>
```
 Bootstrap arbeitet mit 12 Spalten. Wir benutzen 6 Spalten.

Um festzustellen ob das Senden der Daten funktioniert, können wir wieder in der Netzwerkanalyse des Browsers nachsehen.

Script am Ende des Bodys einfügen. Java function `validateExamDate()` erstellen und in der konsole im Browser überprüfen ob diese function nach erneutem Laden der Seite bekannt ist.

ACHTUNG: Funktionen können gecached sein und daher bei neuem Laden evtl. noch nicht erkannt werden. Oder Cache deaktivieren im Netzwerkanalyse Tool des Browsers.

#### Clientseitige Validierung

Formularverarbeitung

PHP unter h1 einfügen.

In php mit `print_r($POST);` können die Felder in der Website angezeigt werden.

Neue func.inc.php erstellen.
In php mit `require "lib/func.inc.php"` inkludieren.

index.php wird zuerst mit GET aufgerufen und beim senden des Formulars ein zwietes mal mit POST aufgerufen.

Formularverarbeitung integrieren:

Achtung: PHP ist eine Skriptsprache und wird Zeile für Zeile interpretiert. Vorsicht bei der Platzierung des PHP-Codes. Es sind auch mehrere PHP-Bereiche in einer Datei möglich.

Erst überprüfen wir ob die POST Methode richtig funktioniert mit: `print_r($POST);`.

Überprüfung ob wir eine GET oder POST Methode haben mit:

``` php
if (isset($_POST['submit'])){
    ...
}
```

Zur Validierung selbst werden wir jetzt Funktionen schreiben und in eine eigene Datei auslagern (func.inc-php).

Das neue Skript müssen wir mit `require` oder `include` in die index.php einbinden.

Bei `require` muss der Pfad stimmen oder das ganze Skript wird abgebrochen.

Wenn der Bösewicht z.B. das Feld `name` auf `name2` ändert, dann wird uns eine Fehlermeödung angezeigt. Also müssen wir vorher wieder überprüfen ob die Variable `name` auch vorhanden ist mit `$name = isset($_POST['name']) ? $_POST['name'] : '';`. Dasselbe machen wir für alle anderen Variablen und anschließend können wir die Variablen validieren.

Damit die Eingabe in die Felder nicht einfach verschwinden, geben wir die Werte wieder aus mit: `value:`

Achtung: Der Reset-Button eines Standardformulars bezieht sich nur auf das eingetragene Value. Nicht aber das vorprogrammierte Value im Code.

Im Code kann man mit `value=<?php echo $name; ?>` oder mit `value="<?= $name?>"` der Standard für die Variable festgelegt werden. Dieser `name` wird 1:1 aus dem Eingabefeld übernommen, ohne überprüfung auf Schadcode

Hier muss man darauf achten jede Eingabe auf Schadcode zu prüfen mit `htmlspecialchars($name)`.

Da die Variablen im $_POST erst erstellt werden, mussen wir diese vor der if-Anweisung deklarieren.

Weil wir das `value` Feld gesetzt haben, werden die gesendeten Daten wieder in die Eingabefelder eingesetzt.

Da die select-Box kein Value hat sondern Optionen, müssen wir überprüfen welche Option ausgewählt wurde. Jede `option` braucht ein `value`! Das ausgewählte `value` wird dann im `$_POST` mitgeschickt.

Mit `<option value="" hidden>- Fach auswählen -</option>` wird eine nicht auswählbare Anzeige im `select` Feld angezeigt.

In der `select` Box müssen wir überprüfen ob ein `value` gesetzt ist mit `<option value="m" <?php if ($subject == 'm') echo "selected='selected'"; ?>>Mathematik</option>`


#### Serverseitige Validierung

In der if-Anweisung der index.php:

![Alt text](image.png)

Dann schreiben wir die `validate` Funktionen:

Als erstes erstellen wir ein assoziatives Array für die Fehlermeldungen und dann müssen wir bei allen Prüfungen die `$errors` Variable als `global $errors;` angeben.

`$errors = [];`

Name:

![Alt text](image-2.png)

Dann müssen wir noch die Klasse form-controll ändern:

`form-control <?= isset($errors=['name']) ? 'is-invalid' : '' ?>`


#### Tests und Erweiterungen

Testprotokoll erstellen um sicher zu stellen dass alles funktioniert.

### Clientseitige Validierung

Alles richtig ausgefüllt:

![Alt text](image-3.png)

Name leer:

![Alt text](image-4.png)

Email leer => Optional also korrekt:

![Alt text](image-5.png)

Kein Fach ausgewähtl und Datum in der Zukunf Fehler:

![Alt text](image-6.png)

Note auserhalb des erlaubten Bereichs:

![Alt text](image-7.png)

Alle Felder löschen funktioniert:

![Alt text](image-8.png)

### Serverseitige Validierung

Name länger als 20 Zeichen => Serverseitiger Fehler:

![Alt text](image-9.png)

Email nicht im richtigen Format:

![Alt text](image-10.png)

Bei Note einen Text eingegeben statt einer Zahl zwischen 0 und 6:

![Alt text](image-11.png)

Bei Datum einen Text eingegeben statt eines Datums:

![Alt text](image-12.png)



# Philip Ackermann Fullstack-Entwicklung Kapitel 2 (Seite 23 - 27)

## 2.2.6 Formulare definieren

| Element                | Beschreibung                                                                                                          |
|------------------------|-----------------------------------------------------------------------------------------------------------------------|
| Textfelder             | zur Eingabe von Texten, beispielsweise bei Suchmaschinen bei Eingabe des Suchbegriffs.                                |
| Passwortfelder         | zur Eingabe von Passwörtern, beispielsweise bei Login-Formularen. Zeichen werden verdeckt angezeigt.                   |
| Textbereiche           | zur Eingabe von längeren Texten, beispielsweise wenn Sie in einem sozialen Netzwerk einen Beitrag formulieren.        |
| Radiobuttons           | zur Auswahl einer von mehreren Optionen, beispielsweise wenn Sie bei einer Onlinebestellung zwischen Lieferanten wählen können. |
| Checkboxen             | zur Auswahl von mehreren Optionen, beispielsweise wenn Sie bei einer Onlinebestellung die AGB und Datenschutzrichtlinien bestätigen. |
| Auswahllisten          | zur Auswahl einer Option aus einer Liste, beispielsweise bei Eingabe des Wohnortes aus einer vorgeschlagenen Liste.    |
| Dateiuploads           | zum Hochladen von Dateien, beispielsweise wenn Sie in einem sozialen Netzwerk ein neues Foto hochladen.                |
| Schaltflächen/Buttons  | zum Absenden eines Formulars.                                                                                         |

- Das `<form>`-Element definiert ein Formular.
- Mit `<fieldset>` können Bereiche in einem Formular gruppiert werden, was die Benutzerfreundlichkeit und Barrierefreiheit erhöht.
- Eingabefelder werden mit dem `<input>`-Element erstellt. Der Typ des Eingabefeldes wird über das type-Attribut festgelegt:
    - Textfelder: type="text"
    - Passwortfelder: type="password"
    - Radiobuttons: type="radio"
    - Checkboxen: type="checkbox"
    - Für E-Mail-Adressen: type="email" (der Browser prüft, ob es eine gültige E-Mail-Adresse ist).
- Dropdownlisten werden mit dem `<select>`-Element erstellt, wobei die einzelnen Optionen mit dem `<option>`-Element definiert werden.
- Große Textbereiche werden mit dem `<textarea>`-Element erstellt.
- Um einem Eingabefeld eine Beschreibung hinzuzufügen, wird das `<label>`-Element verwendet. Das `<label>` kann direkt mit dem `<input>` verknüpft werden oder separat platziert und über das for-Attribut mit dem zugehörigen Formularelement verbunden werden, indem man die ID des Formularelements angibt.

Beim Absenden eines HTML-Formulars werden Daten an einen Webserver gesendet. Hier sind die wichtigsten Punkte zur Handhabung dieser Daten:

- Das action-Attribut des `<form>`-Elements definiert die URL, an die die Formulardaten gesendet werden. Ein Beispiel im Text verwendet den relativen URL-Pfad "/services/handle-form". An diese URL wird typischerweise ein Webservice angeschlossen, der die Daten empfängt, verarbeitet und eine Bestätigungsseite zurückgibt.

- Das method-Attribut des `<form>`-Elements bestimmt die HTTP-Methode (z.B. GET oder POST), die zum Senden der Formulardaten verwendet wird.

- Formulardaten werden als Paare aus Name und Wert an den Server gesendet. Der Name eines Formularelements wird über das name-Attribut festgelegt.

- Zum Absenden des Formulars wird eine Schaltfläche benötigt. Dies wird oft mit dem `<input>`-Element und dem Wert submit für das type-Attribut realisiert. Ein Klick darauf sendet das Formular an die im action-Attribut angegebene URL.


# Philip Ackermann Fullstack-Entwicklung Kapitel 4 (Seite 1 - 26)

## JavaScript einbinden

JavaScript kann auf verschiedene Weise eingebunden werden.

### Verzeichnisstruktur

- Projekt
  - index.html
  - scripts
    - main.js
  - styles
    - main.js

### JavaScript in eine HTML-Datei einbinden

``` html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Beispiel</title>
    <link rel="stylesheet" href="styles/main.css" type="text/css">
</head>
<body>
    <!--Hier wird die JavaScript-Datei eingebunden -->
    <script src="scripts/main.js"></script>
</body>
</html>
```

### JavaScript direkt innerhalb einer HTML-Datei definieren

``` html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Beispiel</title>
    <link rel="stylesheet" href="styles/main.css" type="text/css">
</head>
<body>
    <!--Hier wird die JavaScript-Datei eingebunden -->
    <script>
        alert('Hallow Welt');
    </script>
</body>
</html>
```

### Dialogfenster anzeigen

- Mit der Funktion alert() kann man einen einfachen Hinweisdialog erstellen.
- Die Funktion confirm() zeigt einen Bestätigungsdialog an, bei dem der Benutzer zwischen zwei Optionen wählen kann: Bestätigen und Abbrechen.
- Mit prompt() lässt sich ein Eingabedialog anzeigen, bei dem der Benutzer einen Text eingeben kann.

Standarddialoge werden selten genutzt, da ihre Funktionalität sehr eingeschränkt ist.

### Die Entwicklerkonsole verwenden

Browserspezifisches öffnen der Konsole.

#### Verwendung des console-Objekts

```javascript
console.log('Hallo Entwicklerwelt');  // Ausgabe einer normalen Meldung
console.debug('Hallo Entwicklerwelt');  // Ausgabe einer Debug-Meldung
console.error('Hallo Entwicklerwelt');  // Ausgabe einer Fehlermeldung
console.info('Hallo Entwicklerwelt');  // Ausgabe einer Infomeldung
console.warn('Hallo Entwicklerwelt');  // Ausgabe einer Warnung
```

| Methode  | Beschreibung |
|----------|--------------|
| `clear()`| Leert alle Meldungen auf der Konsole. |
| `debug()`| Gibt die Meldung nur dann aus, wenn das Log-Level »debug« aktiviert ist, also nur während des Debuggens. |
| `error()`| Gibt eine Fehlermeldung aus. In den meisten Browsern wird in der Konsole ein Fehler-Icon neben der ausgegebenen Meldung angezeigt. |
| `info()` | Gibt eine Infomeldung auf die Konsole aus. |
| `log()`  | Gibt eine »normale« Meldung auf der Konsole aus. |
| `warn()` | Dient dazu, eine Warnung auf der Konsole auszugeben. Auch hier wird in den meisten Browsern ein entsprechendes Icon neben der Meldung ausgegeben. |

## Variablen, Konstanten, Datentypen und Operatoren

### Variablen definieren

``` javascript
let firstName;                  // Variablendeklaration
firstName = 'Max';              // Variableninitialisierung
let lastName = 'Mustermann';    // Kombinierte Variablendeklaration und Variableninitialisierung

console.log(firstName);         // "Max"
console.log(lastName);          // "Mustermann"
```

### Konstanten definieren

``` javascript
const MAXIMUM = 5000;
```

### Datentypen verwenden

#### Zahlen

``` javascript
const number1 = 5;     // Definition einer Ganzzahl
const number2 = 0.5;   // Definition einer Kommazahl
const number3 = -22;   // Definition einer negativen Ganzzahl
const number4 = -0.9;  // Definition einer negativen Kommazahl
```

#### Zeichen

``` javascript
const firstName = 'Max';               // einfache Anführungszeichen
const lastName = "Mustermann";         // doppelte Anführungszeichen
const age = "22";                      // keine Zahl, sondern Zeichenkette
const street = 'Musterstraße';         // Syntaxfehler: Mischform
```

#### Boolean

``` javascript
const isLoggedIn = true;
const isAdmin = false;
```

### Operatoren verwenden

1. **Arithmetische Operatoren**
    - Erlauben grundlegende Rechenoperationen.
    - Beispiele:
        - `+` : Addition
        - `-` : Subtraktion
        - `*` : Multiplikation
        - `/` : Division
        - `%` : Modulo (Restdivision)
        - `**`: Exponentiation
        - `++`: Inkrement
        - `--`: Dekrement

2. **Zuweisungsoperator**
    - Verwendet, um Variablen Werte zuzuweisen.
    - Beispiel:
        - `=` : Zuweisung

3. **Verkettungsoperator**
    - Verbindet zwei Zeichenketten.
    - Beispiel:
        - `+` : Verkettung

4. **Logische Operatoren**
    - Prüfen boolesche Bedingungen.
    - Beispiele:
        - `&&`: Logisches UND
        - `||`: Logisches ODER
        - `!` : Negation

5. **Bitweise Operatoren**
    - Arbeiten auf Bit-Ebene.
    - Beispiele:
        - `&` : Bitweises UND
        - `|` : Bitweises ODER
        - `^` : Bitweises XOR
        - `~` : Bitweise Negation
        - `<<`: Linksverschiebung
        - `>>`: Rechtsverschiebung

6. **Vergleichsoperatoren**
    - Vergleichen Werte und liefern boolesche Ergebnisse.
    - Beispiele:
        - `<`  : Kleiner als
        - `>`  : Größer als
        - `<=` : Kleiner oder gleich
        - `>=` : Größer oder gleich
        - `==` : Gleichheit
        - `===`: Strikte Gleichheit (Wert und Typ)
        - `!=` : Ungleichheit
        - `!==`: Strikte Ungleichheit

## Kontrollstrukturen verwenden

### Bedingte Anweisungen und Verzweigungen verwenden

#### Verzweigung

```javascript
let passwordTooShort = password.length < 10;
let passwordTooLong = password.length > 50;
let message = '';
if (passwordTooShort) {
    message = 'Das Passwort muss mindestens 10 Zeichen enthalten.';
} else if (passwordTooLong) {
    message = 'Das Passwort darf maximal 50 Zeichen enthalten.';
} else {
    message = 'Das Passwort erfüllt alle Bedingungen.';
}
document.getElementById('info').textContent = message;
```

#### Switch-Anweisung

``` javascript
const testResultElement = document.getElementById('testResult');
const testResult = 0; // Testergebnis, würde normalerweise berechnet
let icon = null;      // Variablene, die den Bildnamen enthalten wird

switch(testResult) {
    case 0:
        icon = 'pass.png'; // Hat dieses den Wert 0,
        break;             // Abbruch der switch-Anweisung
    case 1:
        icon = 'info.png'; // Hat das Testergebnis den Wert 1,
        break;             // Abbruch der switch-Anweisung
    case 2:
        icon = 'warning.png'; // Hat das Testergebnis den Wert 2,
        break;                // Abbruch der switch-Anweisung
    case 3:
        icon = 'error.png'; // Hat das Testergebnis den Wert 3,
        break;              // Abbruch der switch-Anweisung
    default:
        icon = 'unknown.png'; // Für alle anderen Werte
}

testResultElement.src = 'img/' + icon;
```

### Schleifen verwenden

#### Einfache FOR Schleife

``` javascript
for (let i = 1; i <= 10; i++) {
    console.log(i);
}
```

#### Einfache WHILE Schleife

``` javascript
let i = 1;
while (i <= 10) {
    console.log(i);
    i++;
}
```

#### Einfache DO-WHILE Schleife

``` javascript
let i = 1;
do {
    console.log(i);
    i++;
} while (i <= 10);
```

## Funktionen und Fehlerbehandlung

### Funktionen definieren uund aufrufen

#### Funktionsdeklaration

``` javascript
function printNumbersFrom1To10() {
    for (let i = 1; i <= 10; i++) {
        console.log(i);
    }
}
```

#### Funktionsausdruck (Function Expression)

``` javascript
const printNumbersFrom1To10 = function() {
    for (let i = 1; i <= 10; i++) {
        console.log(i);
    }
}
```

#### Arrow Function (Pfeilfunktion)

``` javascript
const printNumbersFrom1To10 = () => {
    for (let i = 1; i <= 10; i++) {
        console.log(i);
    }
}
```

### Funktionsparameter übergeben

``` javascript
function printNumbersFromXToY(x, y) {
    for (let i = x; i <= y; i++) {
        console.log(i);
    }
}

printNumbersFromXToY(1, 10);    // Zahlen 1 bis 10
printNumbersFromXToY(1, 100);  // Zahlen 1 bis 100
printNumbersFromXToY(100, 1000);  // Zahlen 100 bis 1000
```

### Rückgabewerte definieren

``` javascript
function sum(x, y) {
    let result = x + y;  // Addition der zwei übergebenen Parameter
    return result;      // Rückgabe des Ergebnisses
}

const z = sum(5, 6);  // Aufruf und Zuweisung des Rückgabewertes
console.log(z);       // 11
```

### Auf Fehler reagieren

Bei der Ausführung von Programmen können verschiedene Fehlerarten auftreten:

- **Syntaxfehler**: Tritt auf, wenn die syntaktischen Regeln von JavaScript nicht befolgt werden, z.B. wenn Klammern bei der Definition einer Funktion vergessen werden.
  
- **Laufzeitfehler**: Fehler, die nicht durch fehlerhafte Syntax verursacht werden, sondern wenn z.B. auf nicht vorhandene Variablen zugegriffen wird.

- **Logische Fehler/Bugs**: Fehler aufgrund einer falschen Programmlogik.

Syntaxfehler können durch Editoren oder Entwicklungsumgebungen mit `Syntaxvalidierung` identifiziert werden.

Logische Fehler können mit automatisierten Tests vermieden werden.

Um auf Laufzeitfehler zu reagieren, gibt es die `try-catch`-Codeblöcke:

- **try**: Ein Codeblock, der potenziell Fehler produzieren könnte.
  
- **catch**: Fängt den Fehler auf und ermöglicht eine Reaktion darauf. Es bietet Zugriff auf ein Objekt mit Details zum Fehler.

Beispiel:

```javascript
function checkPassword(password) {
    if (password.length < 10) {
        throw new Error('Das Passwort muss mindestens 10 Zeichen enthalten.');
    } else if (password.length > 50) {
        throw new Error('Das Passwort darf maximal 50 Zeichen enthalten.');
    }
    return 'Das Passwort erfüllt alle Bedingungen.';
}

try {
    const password = 'simple';
    checkPassword(password);
    const message = 'Das Passwort erfüllt alle Bedingungen.';
    // ... hier weitere Verarbeitung ...
} catch (error) {
    console.error(error);
}
```

## Objekte und Arrays

### Objekte verwenden

``` javascript
const book = {
    title: 'Schrödinger programmiert Java',
    price: 44.90,
    author: 'Philip Ackermann',
    isbn: '978-3836245838978-3836245838',
    printDescription() {
        console.log(`${this.author}: ${this.title}`);
    }
}

console.log(book.title);       // "Schrödinger programmiert Java"
console.log(book.price);       // 44.9
console.log(book.author);      // "Philip Ackermann"
console.log(book.isbn);        // "978-3836245838"
book.printDescription();       // "Philip Ackermann: Schrödinger programmiert Java"
```

### Arrays verwenden

Der Index von Arrays beginnt bei 0.

#### Erzeugen eines Arrays (Kurzschreibweise)

``` javascript
// Erzeugen eines Arrays mit bestimmten Werten
const names = ['Max', 'Moritz', 'Peter'];
// Zugriff auf die Elemente eines Arrays
console.log(names[0]); // "Max"
console.log(names[1]); // "Moritz"
console.log(names[2]); // "Peter"

// Erzeugen eines leeren Arrays
const colors = [];
// Hinzufügen von Werten
colors.push('red');
colors.push('green');
colors.push('blue');
// Hinzufügen von Werten über Index-Schreibweise
colors[3] = 'orange';
colors[4] = 'yellow';
console.log(colors); // ["red", "green", "blue", "orange", "yellow"]
```

#### Iteration über Elemente eines Arrays

``` javascript
for (let i=0; i<colors.length; i++) {
    console.log(colors[i]);
}
```

#### Methoden von Arrays

| Methode   | Beschreibung                                                                   |
|-----------|--------------------------------------------------------------------------------|
| `concat()`| Hängt Elemente oder Arrays an ein bestehendes Array an.                         |
| `filter()`| Filtert Elemente aus dem Array auf Basis eines in Form einer Funktion übergebenen Filterkriteriums. |
| `forEach()`| Wendet eine übergebene Funktion auf jedes Element im Array an.                   |
| `join()`  | Wandelt ein Array in eine Zeichenkette um.                                      |
| `map()`   | Bildet die Elemente eines Arrays auf Basis einer übergebenen Umwandlungsfunktion auf neue Elemente ab. |
| `pop()`   | Entfernt das letzte Element eines Arrays.                                       |
| `push()`  | Fügt ein neues Element am Ende des Arrays ein.                                  |
| `reduce()`| Fasst die Elemente eines Arrays auf Basis einer übergebenen Funktion zu einem Wert zusammen. |
| `reverse()`| Kehrt die Reihenfolge der Elemente im Array um.                                 |
| `shift()` | Entfernt das erste Element eines Arrays.                                        |
| `slice()` | Schneidet einzelne Elemente aus einem Array heraus.                              |
| `splice()`| Fügt neue Elemente an beliebiger Position im Array hinzu.                        |
| `sort()`  | Sortiert das Array, optional auf Basis einer übergebenen Vergleichsfunktion.    |
| `unshift()`| Fügt neue Element an das Ende eines Arrays hinzu.                               |

### Zusammenfassung

- **Einbindung**: JavaScript kann auf unterschiedliche Weise in Websites eingebunden werden. Optimal ist die Nutzung separater Dateien.
  
- **Variablen**: 
  - Mit `let` für veränderbare Variablen.
  - Mit `const` für konstante Werte.
  
- **Datentypen**: 
  - Primitive: Zahlen, Zeichenketten, boolesche Werte.
  - Besondere: `undefined` und `null`.
  - Objekte.
  
- **Operatoren**: 
  - Arithmetische, für Zeichenketten, boolesche Werte, Bits, zum Vergleichen von Werten und Typüberprüfung.
  
- **Kontrollfluss**: Durch bedingte Anweisungen, Mehrfachverzweigungen, Schleifen usw.
  
- **Funktionen**: Wiederverwendbare Programmteile mit optionalen Argumenten und Rückgabewerten.
  
- **Fehlerarten**:
  - Syntaxfehler: Bei Verstoß gegen JS-Regeln.
  - Laufzeitfehler: Tritt während der Ausführung auf.
  - Logische Fehler: Durch inkorrekte Logik im Code.

- **Interaktive Webseiten**: Fehlerbehandlung mit `try` und `catch` und dynamische Erstellung von HTML-Code.

---

# Philip Ackermann Fullstack-Entwicklung Kapitel 5 (Seite 1 - 19)

## Hypertext Transfer Protocol

HTTP ist ein sogenanntes Client/Server-Protokoll wobei die initiale Kommunikation vom Client ausgeht. Jedes Bild, jede CSS-Datei und jede JavaScript-Datei, die in eine Website eingebunden ist, führt zu einer seperaten Anfrage, die vom Browser an den Server geschickt und von diesem bearbeitet wird.

### Request und Response

![Alt text](image-13.png)

![Alt text](image-14.png)

#### HTTP-Anfrage und HTTP-Antwort

- **Start Line**: Auch als "Status Line" oder "Initial Line" bekannt. Beschreibt den Request oder die Response.
  
- **Header**: Optionale Zeilen, die entweder als "HTTP-Request-Header" oder "HTTP-Response-Header" bezeichnet werden. Diese beschreiben den Request oder die Response genauer.

- **Meta-Informationen**: Start Line und Header zusammen.

- **Body**: Auch als "Payload" bekannt. Dies ist der Nachrichtenkörper. Beim Request kann es sich um Formulardaten handeln, während bei einer Response es beispielsweise ein HTML-Dokument, Stylesheet oder JavaScript-Code sein kann.

![Alt text](image-15.png)

### Aufbau von Anfragen (HTTP-Requests)

- **Definition**: Bestimmt die Aktion, die auf dem Server ausgeführt werden soll.
  
- **Verfügbare Methoden**: 
  - `GET`: Startet eine Anfrage, um Daten vom Server zu laden (z.B. Bilder oder CSS).
  - `POST`: Startet eine Anfrage, um Daten an den Server zu senden (z.B. Formulare).
  - Andere Methoden: `PUT`, `DELETE`, `CONNECT`, `TRACE`, `HEAD`, `OPTIONS`.
  
- **URL**: Nach der HTTP-Methode folgt das Ziel der Anfrage, entweder als absolute oder relative URL.

- **HTTP-Version**: Bestimmt, welche Version von HTTP für die Anfrage verwendet wird.

![Alt text](image-16.png)

### Aufbau von Antworten (HTTP-Response)

- **HTTP-Version**: Am Anfang jeder Antwort angegeben.

- **Statuscode**: Gibt an, ob die Bearbeitung der Anfrage erfolgreich oder fehlerhaft war. Zum Beispiel zeigt der Statuscode `404` an, dass eine Ressource nicht gefunden wurde.

- **Statustext**: Textuelle Beschreibung des Statuscodes. Für `404` wäre es beispielsweise "Not Found".

![Alt text](image-17.png)

### Header

HTTP-Header übermitteln Meta-Informationen für Anfragen und Antworten. Sie definieren das Datenformat für Anfragen und erwartete Antwortformate.

### Methoden

1. **GET**: Dient zum Abrufen von Daten vom Server. Oft verwendet, wenn Webseiten geöffnet werden.
  
2. **POST**: Überträgt Daten an den Server. Wird bspw. für Formulare oder explizite POST-Anfragen in JavaScript genutzt.

3. **PUT**: Aktualisiert Ressourcen auf dem Server. Häufig durch JavaScript umgesetzt, da nicht alle Browser PUT unterstützen.

4. **DELETE**: Löscht Ressourcen auf dem Server. In der Praxis selten direkt über den Browser genutzt.

5. **PATCH**: Führt punktuelle Änderungen an einer Ressource durch.

6. **HEAD**: Ähnlich wie GET, gibt jedoch nur die HTTP-Header zurück. Wird selten verwendet, oft durch automatisierte Tools.

7. **OPTIONS**: Ermöglicht das Abfragen zulässiger Methoden für eine URL.

8. **TRACE**: Kann zur Diagnose verwendet werden, indem Anfragen durch Proxys verfolgt werden. Sicherheitsrelevanz.

9. **CONNECT**: Stellt eine Netzwerkverbindung über einen HTTP-Proxy her, oft für HTTPS-Tunneling verwendet.

> Hinweis: Für eine detailliertere Erläuterung und Anwendung dieser Methoden siehe spezifische Kapitel oder Abschnitte.

### HTTP Statuscodes

Ein HTTP-Statuscode gibt Auskunft darüber, ob eine HTTP-Anfrage vom Server erfolgreich bearbeitet wurde oder nicht. Jeder Statuscode ist eine dreistellige Zahl, gefolgt von einer kurzen Beschreibung. Es gibt fünf Hauptkategorien von Statuscodes, die jeweils durch ihr erstes Ziffern repräsentiert werden.

#### Hauptkategorien von Statuscodes

| Statuscode-Kategorie | Beschreibung                                                      |
|----------------------|-------------------------------------------------------------------|
| 1xx                  | Anfrage wurde entgegengenommen und wird weiterverarbeitet.        |
| 2xx                  | Anfrage wurde erfolgreich vom Server bearbeitet.                  |
| 3xx                  | Anfrage erfordert weitere Aktionen.                               |
| 4xx                  | Anfrage konnte wegen eines Clientseitigen Fehlers nicht bearbeitet werden. |
| 5xx                  | Anfrage konnte wegen eines Serverseitigen Fehlers nicht bearbeitet werden. |

#### Auswahl häufig verwendeter Statuscodes

| Statuscode | Bezeichnung           | Beschreibung                                                                      |
|------------|------------------------|-----------------------------------------------------------------------------------|
| 200        | OK                     | Die Anfrage konnte vom Server erfolgreich bearbeitet werden.                      |
| 301        | Moved Permanently      | Der angefragten Ressource wurde dauerhaft eine neue URL zugewiesen.               |
| 400        | Bad Request            | Fehlerhafte Anfrage.                                                              |
| 401        | Unauthorized           | Anfrage kann ohne Autorisierung nicht bearbeitet werden.                          |
| 403        | Forbidden              | Zugriff auf die angefragte Ressource ist verboten.                                |
| 404        | Not Found              | Die angefragte Ressource wurde nicht gefunden.                                    |
| 405        | Method Not Allowed     | Die verwendete HTTP-Methode ist für die angefragte Ressource nicht erlaubt.       |
| 500        | Internal Server Error  | Interner Serverfehler, aufgrund dessen die Anfrage nicht bearbeitet werden kann.  |

### MIME-Typen (Multipurpose Internet Mail Extension)

MIME-Typen (Multipurpose Internet Mail Extension Types) definieren, welche Art von Daten über HTTP gesendet werden. Sie bestehen aus einem Typ und einem Subtyp, getrennt durch einen Schrägstrich (z.B. `type/subtype`). Der Typ beschreibt die allgemeine Datenkategorie wie Text, Audio oder Video. Der Subtyp spezifiziert die genaue Art der Daten, wie CSS, HTML oder einfacher Text für Textdaten. Zusätzliche Parameter können an MIME-Typen angehängt werden, um weitere Details über die Daten zu definieren, beispielsweise den Zeichensatz eines Textes (`text/plain;charset=UTF-8`).

### Cookies

Das HTTP-Protokoll ist zustandslos, wodurch Server Anfragen unabhängig voneinander behandeln und nicht erkennen können, ob ein Client bereits zuvor eine Anfrage gestellt hat. Um dennoch Nutzeraktionen wie Logins oder den Inhalt von Warenkörben auf Webseiten über mehrere Sessions hinweg zu verfolgen, werden Cookies verwendet. Damit können Websites Informationen über Besuche speichern und erkennen, wenn ein Nutzer zurückkehrt.

![Alt text](image-18.png)

Cookies speichern kleine Mengen von Informationen auf der Clientseite. Sie bestehen aus Schlüssel-Wert-Paaren und werden vom Browser als Textdateien gespeichert. Beim erneuten Besuch einer Webseite werden diese Informationen an den Server gesendet, vorausgesetzt die Browsereinstellungen erlauben dies.

![Alt text](image-19.png)

#### Cookies enthalten folgende Informationen:

1. Cookies haben einen Namen und einen Wert, wobei die Groß- und Kleinschreibung unterschiedlich sein kann. Werte sind oft Strings und nicht z.B. boolesche Werte.
2. Optional können Cookies die Domäne und den Pfad des Servers spezifizieren, an den sie gesendet werden. Sie werden nur bei passenden HTTP-Anfragen mitgesendet.
3. Ein Ablaufdatum bestimmt, wann ein Cookie verfällt. Ohne dieses Datum wird das Cookie beim Schließen des Browsers gelöscht.
4. Ein Sicherheitsflag kann festlegen, ob ein Cookie nur bei sicheren SSL-Verbindungen gesendet wird.

#### Vorteile von Cookies:

1. **Sitzungserhaltung**: Ermöglicht es Benutzern, eingeloggt zu bleiben oder Einkaufswagen-Informationen zwischen den Sitzungen zu speichern.
2. **Benutzerfreundlichkeit**: Speichert persönliche Einstellungen und Vorlieben für bestimmte Webseiten, um das Nutzererlebnis zu verbessern.
3. **Zielgerichtete Werbung**: Ermöglicht Websites und Werbetreibenden, relevantere Anzeigen basierend auf dem Browsing-Verhalten des Benutzers anzuzeigen.
4. **Traffic-Analyse**: Hilft Webseiten-Betreibern, die Nutzung ihrer Seiten zu verstehen und diese entsprechend zu optimieren.

#### Nachteile von Cookies:

1. **Datenschutzbedenken**: Cookies können benutzt werden, um das Verhalten der Benutzer über verschiedene Websites hinweg zu verfolgen.
2. **Sicherheitsrisiken**: Unzureichend geschützte Cookies können von Angreifern ausgenutzt werden, um sensible Benutzerinformationen zu stehlen.
3. **Speicherplatz**: Obwohl sie klein sind, können viele Cookies den Speicherplatz eines Browsers füllen, was zu Performance-Problemen führen kann.
4. **Kurzlebigkeit**: Einige Cookies können nach kurzer Zeit ablaufen, was bedeutet, dass Daten verloren gehen können, wenn sie nicht rechtzeitig gesichert werden.

### HTTP über die Kommandozeile

Mit `curl` kann man HTTP-Anfragen direkt auf der Kommandozeile ausführen.

``` bash
curl -s -v -o /dev/null https://www.rheinwerk-verlag.de/
```

- \-v (--verbose) gibt detaillierte Informationen über die Anfrage und Antwort.
- \-o (--outpput) leitet die Antwort in eine Datei um.
- \-s (--silent) führt den Befehl im stillen Modus aus, ohne Fehlermeldungen oder Fortschrittsanzeigen.

---

# Philip Ackermann Fullstack-Entwicklung Kapitel 7 (Seite 1 - 13)

Die **Document Object Model (DOM)** **Application Programming Interface (API)** ist eine Programmierschnittstelle, die Webdokumente als Baumstruktur von Objekten repräsentiert, wobei jedes Objekt einem Teil des Dokuments entspricht. Diese Objekte können mit Skriptsprachen wie JavaScript manipuliert werden.

## Webseiten dynamisch ändern mit der DOM API

1. **Grundkonzept**: 
   - Der DOM repräsentiert ein Dokument als Baum von Knoten (=nodes). Jeder Knoten entspricht einem Teil des Dokuments, z. B. einem Element oder einem Textinhalt.

    ![Alt text](image-20.png)

2. **Knotentypen**:
   - Elementknoten: Meistens HTML-Tags wie `<div>`, `<a>`, etc.
   - Textknoten: Der Text innerhalb der Elemente.
   - Attributknoten: Die Attribute eines Elements (z. B. `class`, `id`).

    ![Alt text](image-21.png)
   
3. **Hauptmethoden & Eigenschaften**:
   - `document.getElementById(id)`: Wählt ein Element mit einer bestimmten ID aus.
   - `document.getElementsByTagName(name)`: Wählt alle Elemente mit einem bestimmten Tag-Namen aus.
   - `document.createElement(name)`: Erstellt ein neues Element.
   - `parentNode.appendChild(node)`: Fügt einen Knoten als letztes Kind eines anderen Knotens hinzu.
   - `parentNode.removeChild(node)`: Entfernt einen Kindknoten von einem anderen.
   - `element.setAttribute(name, value)`: Legt den Wert eines Attributs für ein Element fest.
   - `element.getAttribute(name)`: Liefert den Wert eines Attributs eines Elements.

4. **Ereignisse**:
   - DOM-Elemente können Ereignis-Listener haben, die aktiviert werden, wenn ein bestimmtes Ereignis eintritt (z. B. Klicks, Tastatureingaben).

5. **Live vs. statische Kollektionen**:
   - Einige DOM-Methoden geben live Kollektionen zurück, die sich automatisch aktualisieren, wenn der DOM verändert wird, während andere statische Kollektionen zurückgeben, die keine Änderungen widerspiegeln.

6. **Browser-Kompatibilität**:
   - Obwohl der DOM standardisiert ist, gibt es Unterschiede in der Implementierung und Unterstützung zwischen verschiedenen Browsern. Es ist daher oft ratsam, Bibliotheken wie jQuery zu verwenden, die diese Unterschiede abstrahieren.

Der DOM ermöglicht es Entwicklern, interaktive und dynamische Webseiten zu erstellen, indem er eine Schnittstelle zur Manipulation von Webinhalten in Echtzeit bietet.

---

# Cookies in PHP

Cookies sind ein wesentliches Element der Webentwicklung und dienen dazu, Nutzerdaten auf deren Geräten zu speichern.

1. **Grundlagen von Cookies**: Cookies sind kleine Datenpakete, die vom Webserver an den Browser des Benutzers gesendet und dort gespeichert werden. Sie werden hauptsächlich verwendet, um Informationen über den Benutzer zu speichern und wiederzuerlangen, z.B. Session-Informationen oder Benutzereinstellungen.

2. **Cookies setzen in PHP**:
   - Verwenden Sie die Funktion `setcookie()`, um ein Cookie zu setzen.
   - Syntax: `setcookie(name, value, expire, path, domain, secure, httponly);`
   - Ein einfaches Beispiel:
     ```php
     setcookie("user", "John Doe", time() + 3600, "/");
     ```
     Dieses Snippet erstellt ein Cookie namens "user" mit dem Wert "John Doe", das nach einer Stunde abläuft.

3. **Cookies abrufen in PHP**:
   - Zugriff auf Cookies erfolgt über das globale `$_COOKIE` Array.
   - Beispiel:
     ```php
     if(isset($_COOKIE["user"])) {
         echo "User: " . $_COOKIE["user"];
     } else {
         echo "Cookie 'user' is not set!";
     }
     ```

4. **Cookies löschen**:
   - Um ein Cookie zu löschen, setzen Sie sein Ablaufdatum in die Vergangenheit.
   - Beispiel:
     ```php
     setcookie("user", "", time() - 3600);
     ```

5. **Sicherheitsaspekte**:
   - Verwenden Sie das `httponly`-Flag, um zu verhindern, dass JavaScript auf das Cookie zugreift.
   - Setzen Sie `secure` auf `true`, wenn das Cookie nur über HTTPS übertragen werden soll.
   - Beispiel:
     ```php
     setcookie("user", "John Doe", time() + 3600, "/", "", true, true);
     ```

6. **Einschränkungen und Best Practices**:
   - Cookies sind auf etwa 4 KB beschränkt.
   - Senden Sie keine sensiblen Informationen in Cookies, es sei denn, sie sind verschlüsselt und gesichert.
   - Beachten Sie die Datenschutzgesetze und Richtlinien, insbesondere die DSGVO in Europa.

Diese Übersicht bietet Ihnen eine solide Grundlage für die Arbeit mit Cookies in PHP.

---

## Session Based Authentication

Im Gegensatz zur Basic Authentication, die bei jeder Anfrage Credentials an den Server sendet, nutzt die Session-Based Authentication Sessions, um den Benutzer zu identifizieren.

Der Ablauf ist wie folgt:

Der Nutzer sendet seine Anmeldedaten (Nutzername und Passwort) an den Server.
Nach erfolgreicher Authentifizierung erstellt der Server eine Session für den Nutzer und generiert eine eindeutige Session-ID, die den Nutzer identifiziert.
Diese Session-ID wird dann als Cookie an den Nutzer zurückgesendet und im Browser gespeichert.
Bei folgenden Anfragen sendet der Nutzer das Cookie mit der Session-ID automatisch mit.
Der Server empfängt das Cookie und kann die im Cookie gespeicherte Session-ID mit den auf dem Server gespeicherten Session-Informationen vergleichen, um die Identität des Nutzers zu überprüfen.

Nach erfolgreicher Authentifizierung werden nicht die Credentials, sondern eine Session-ID bei jeder Anfrage mitgesendet, über die der Server den Nutzer für die Gültigkeit der Session identifizieren kann.

![Session Based Authentication](image-22.png)

---

## Objektorientierte Programmierung

Die Prinzipien der Objektorientierung (OOP) sind grundlegende Konzepte, die die objektorientierte Programmierung definieren und leiten. Hier ist eine kurze Zusammenfassung der Hauptprinzipien:

1. **Kapselung**: Dieses Prinzip bezieht sich auf das Bündeln von Daten und den Methoden, die diese Daten manipulieren, in einer Einheit, bekannt als ein Objekt. Es ermöglicht auch, den Zugriff auf bestimmte Komponenten eines Objekts zu beschränken, was zur Datenintegrität und Sicherheit beiträgt.

2. **Abstraktion**: Abstraktion bedeutet, komplexe Realitäten zu vereinfachen, indem nur die relevanten Daten und Aktionen dargestellt werden. Es ermöglicht dem Programmierer, mit den Konzepten auf einer höheren Ebene statt mit Einzelheiten zu arbeiten.

3. **Vererbung**: Dieses Prinzip erlaubt es, neue Klassen auf Basis bestehender Klassen zu erstellen. Die neue Klasse (abgeleitete Klasse) erbt Eigenschaften und Verhaltensweisen der bestehenden Klasse (Basisklasse), was Code-Wiederverwendung und -Erweiterung erleichtert.

4. **Polymorphismus**: Polymorphismus ist die Fähigkeit, eine Schnittstelle (Methoden) in verschiedenen Formen (Implementierungen) zu nutzen. Es ermöglicht, dass Objekte unterschiedlicher Klassen auf die gleiche Nachricht (Methode) unterschiedlich reagieren können, basierend auf ihrer spezifischen Klassenimplementierung.

Diese Prinzipien bilden das Fundament für das Design und die Struktur von Software in der objektorientierten Programmierung, was zu Code führt, der modularer, wiederverwendbar und einfacher zu verstehen und zu warten ist.

### Kapselung

Kapselung strukturiert die objektorientierte Programmierung. Sie schützt Daten innerhalb von Objekten und steuert den Zugriff darauf.

- **Datenverpackung**: Kapselung bündelt Daten und Methoden in Objekten.

- **Zugriffskontrolle**: Sie definiert, wie und von wem Daten abgerufen oder modifiziert werden.

- **Schnittstellendefinition**: Kapselung trennt die äußere Schnittstelle von der inneren Funktionsweise.

- **Wartung**: Sie erleichtert Änderungen im Code, ohne andere Teile zu beeinflussen.

- **Integrität**: Kapselung wahrt die Datenintegrität durch Verbergen der internen Zustände.

Kapselung ermöglicht robuste und sichere Software-Designs.

### Abstraktion

Abstraktion vereinfacht die objektorientierte Programmierung. Sie fokussiert Entwickler auf wesentliche Merkmale und Funktionen von Objekten, ohne unnötige Komplexität.

- **Essenz**: Abstraktion identifiziert nur notwendige Attribute und Methoden eines Objekts.

- **Allgemeinheit**: Sie ermöglicht die Erstellung genereller Klassen, die Basisfunktionen für verschiedene Objekte bieten.

- **Komplexitätsreduktion**: Sie reduziert Details, was die Entwicklung vereinfacht.

- **Details verstecken**: Interne Klasseninformationen bleiben verborgen, was klarere Schnittstellen schafft.

- **Wiederverwendbarkeit**: Abstrakte Klassen dienen als Vorlagen, um den Code-Einsatz zu optimieren.

Abstraktion erleichtert so die Entwicklung flexibler, wartbarer und erweiterbarer Systeme.

### Vererbung

Vererbung organisiert und optimiert die objektorientierte Programmierung. Sie erlaubt Klassen, Eigenschaften und Methoden zu teilen.

- **Hierarchiebildung**: Vererbung schafft eine Klassenhierarchie von allgemein zu spezifisch.

- **Code-Wiederverwendung**: Sie ermöglicht es, bestehenden Code in neuen Klassen zu nutzen.

- **Erweiterbarkeit**: Neue Klassen können bestehende Funktionalitäten erweitern, ohne diese zu duplizieren.

- **Polymorphismus**: Vererbung unterstützt die Erstellung von Objekten, die mehrere Formen annehmen können.

- **Wartung**: Sie vereinfacht die Aktualisierung und Wartung von Code durch gemeinsame Basislogik.

Vererbung führt zu effizientem und systematischem Code-Aufbau.

### Polymorphie

Polymorphie verfeinert die Objektorientierung durch Flexibilität in der Funktionsausführung.

- **Vielseitige Nutzung**: Polymorphie erlaubt unterschiedliche Implementierungen einer Schnittstelle.
- **Methodenüberladung**: Gleiche Methodennamen erhalten verschiedene Parameterlisten für unterschiedliche Aktionen.
- **Methodenüberschreibung**: Abgeleitete Klassen ändern spezifisch das Verhalten geerbter Methoden.
- **Dynamische Bindung**: Objekte entscheiden zur Laufzeit, welche Methode aufgerufen wird.
- **Interoperabilität**: Polymorphie fördert die Zusammenarbeit verschiedener Objekttypen.

Polymorphie ermöglicht effiziente und flexible Code-Strukturen.

---

## Klassen und Objekte verwenden

In PHP werden Klassen und Objekte verwendet, um Code zu strukturieren und die Prinzipien der objektorientierten Programmierung anzuwenden. Hier ist eine grundlegende Beschreibung mit Code-Snippets:

### Definition einer Klasse

Eine Klasse in PHP wird definiert, indem das Schlüsselwort `class` gefolgt vom Namen der Klasse verwendet wird. Innerhalb der Klasse definieren Sie Eigenschaften (Variablen) und Methoden (Funktionen).

```php
class Auto {
    public $farbe;
    public $marke;
    
    public function __construct($farbe, $marke) {
        $this->farbe = $farbe;
        $this->marke = $marke;
    }
    
    public function beschreibung() {
        return "Ein " . $this->farbe . " " . $this->marke . ".";
    }
}
```

### Erstellen eines Objekts

Ein Objekt ist eine Instanz einer Klasse. Sie erstellen ein Objekt, indem Sie das `new`-Schlüsselwort verwenden, gefolgt vom Namen der Klasse.

```php
$meinAuto = new Auto("rot", "Volkswagen");
```

### Zugriff auf Eigenschaften und Methoden

Nachdem Sie ein Objekt erstellt haben, können Sie auf seine öffentlichen Eigenschaften und Methoden mit dem Pfeiloperator (`->`) zugreifen.

```php
echo $meinAuto->beschreibung(); // Gibt aus: Ein rot Volkswagen.
```

### Beispielcode

Hier ist ein komplettes Beispiel, das die Definition einer Klasse, das Erstellen eines Objekts und den Zugriff auf dessen Eigenschaften und Methoden zeigt:

```php
<?php
class Auto {
    public $farbe;
    public $marke;
    
    public function __construct($farbe, $marke) {
        $this->farbe = $farbe;
        $this->marke = $marke;
    }
    
    public function beschreibung() {
        return "Ein " . $this->farbe . " " . $this->marke . ".";
    }
}

// Objektinstanz erstellen
$meinAuto = new Auto("rot", "Volkswagen");

// Eigenschaft und Methode des Objekts verwenden
echo $meinAuto->beschreibung(); // Gibt aus: Ein rot Volkswagen.
?>
```

In diesem Beispiel definiert die Klasse `Auto` ein Konstrukt mit zwei Eigenschaften: `$farbe` und `$marke`. Der Konstruktor wird aufgerufen, wenn ein neues Objekt erstellt wird, und initialisiert die Eigenschaften des Objekts. Die Methode `beschreibung` gibt eine formatierte Zeichenkette zurück, die das Auto beschreibt.

### Klassenkonstanten

Klassenkonstanten sind Werte, die fest innerhalb einer Klasse definiert sind und nicht verändert werden können. Sie sind über die Klasse selbst zugänglich, nicht über eine Instanz der Klasse.

```php
class Auto {
    const RÄDER = 4;
    
    public function anzahlRäder() {
        return self::RÄDER;
    }
}

echo Auto::RÄDER; // Gibt aus: 4
```

### Sichtbarkeit

Die Sichtbarkeit von Eigenschaften und Methoden in PHP wird durch die Schlüsselwörter `public`, `protected` oder `private` gesteuert. `public` bedeutet, dass von überall darauf zugegriffen werden kann, `protected` erlaubt den Zugriff nur innerhalb der Klasse und abgeleiteten Klassen, und `private` erlaubt den Zugriff nur innerhalb der Klasse selbst.

```php
class Auto {
    public $farbe; // Überall zugänglich
    protected $motor; // Zugänglich innerhalb der Klasse und abgeleiteten Klassen
    private $geheimnis; // Nur innerhalb dieser Klasse zugänglich
}
```

### Vererbung

In PHP kann eine Klasse Eigenschaften und Methoden von einer anderen Klasse erben. Das Schlüsselwort `extends` wird verwendet, um Vererbung zu definieren.

```php
class Fahrzeug {
    public $räder = 4;
}

class Auto extends Fahrzeug {
    public function anzahlRäder() {
        return $this->räder;
    }
}

$meinAuto = new Auto();
echo $meinAuto->anzahlRäder(); // Gibt aus: 4
```

### Klassenabstraktion

Abstrakte Klassen sind Klassen, die nicht direkt instanziiert werden können. Sie dienen als Vorlage für andere Klassen. Eine abstrakte Klasse kann abstrakte Methoden enthalten, die in den abgeleiteten Klassen definiert werden müssen.

```php
abstract class Fahrzeug {
    abstract public function hupen();
}

class Auto extends Fahrzeug {
    public function hupen() {
        return "Hup Hup";
    }
}

$meinAuto = new Auto();
echo $meinAuto->hupen(); // Gibt aus: Hup Hup
```

Diese Konzepte sind zentral in der objektorientierten Programmierung mit PHP und ermöglichen eine strukturierte und wartbare Code-Basis.

---

## JSON

JSON (JavaScript Object Notation) ist ein leichtgewichtiges Daten-Austauschformat, das für Menschen leicht zu lesen und für Maschinen einfach zu parsen und zu generieren ist. Es basiert auf einer Teilmenge der JavaScript-Programmiersprache, wird aber aufgrund seiner Textform und Sprachunabhängigkeit in vielen Programmiersprachen einschließlich PHP verwendet.

### Aufbau von JSON

JSON besteht aus zwei Strukturen:

1. **Objekte**: Eine Sammlung von Schlüssel-Wert-Paaren, umgeben von geschweiften Klammern `{}`. Der Schlüssel muss ein String sein, gefolgt von einem Doppelpunkt `:`, und der Wert kann ein String, eine Zahl, ein Array, ein Objekt oder einer der Literale `true`, `false` oder `null` sein.

```json
{
  "name": "John Doe",
  "alter": 30,
  "verheiratet": false,
  "kinder": null
}
```

2. **Arrays**: Eine geordnete Liste von Werten, umgeben von eckigen Klammern `[]`. Werte in einem Array werden durch Kommas getrennt.

```json
[
  "Apfel",
  "Banane",
  "Kirsche"
]
```

### Funktion von JSON

JSON dient als Format, um strukturierte Daten über Netzwerkgrenzen hinweg zu übertragen, wie z.B. zwischen einem Server und einer Webanwendung. Es ist sprachunabhängig und kann von vielen verschiedenen Programmiersprachen leicht verarbeitet werden.

### Verwendung von JSON in PHP

In PHP können Sie mit den Funktionen `json_encode()` und `json_decode()` JSON-Strings erstellen und parsen.

- **`json_encode()`**: Konvertiert einen PHP-Wert in einen JSON-String.

```php
$daten = [
  "name" => "John Doe",
  "alter" => 30,
  "verheiratet" => false,
  "kinder" => null
];

$jsonString = json_encode($daten);
echo $jsonString;
// Gibt aus: {"name":"John Doe","alter":30,"verheiratet":false,"kinder":null}
```

- **`json_decode()`**: Parst einen JSON-String und konvertiert ihn in PHP-Datenstrukturen.

```php
$jsonString = '{"name":"John Doe","alter":30,"verheiratet":false,"kinder":null}';
$phpArray = json_decode($jsonString, true); // true für ein assoziatives Array
print_r($phpArray);
// Gibt aus: Array ( [name] => John Doe [alter] => 30 [verheiratet] => ... )
```

Durch das Konvertieren von komplexen Datenstrukturen zu einem JSON-String und umgekehrt bietet JSON eine sehr handliche Möglichkeit, Daten zwischen verschiedenen Systemen oder Komponenten einer Anwendung zu übertragen.

---

## Kapitel 5 - Webprotokolle verwenden

### 5.1.8 Cookies 

Der Abschnitt beschreibt das Konzept von Cookies im Kontext von HTTP, dem Protokoll, das für die Kommunikation im Web verwendet wird. HTTP ist ein zustandsloses Protokoll, was bedeutet, dass es von Natur aus keine Informationen von einer Anfrage zur nächsten speichert. Cookies werden verwendet, um dieses Limit zu überwinden, indem sie es ermöglichen, Zustandsinformationen zwischen Client und Server zu speichern und zu übertragen.

Cookies sind kleine Datensätze, die auf dem Client gespeichert werden und Informationen in Form von Schlüssel-Wert-Paaren enthalten. Wenn ein Nutzer eine Webseite besucht, kann der Server bestimmte Daten als Cookie auf dem Browser des Nutzers speichern, die dann bei nachfolgenden Anfragen an den Server zurückgesendet werden können. Dies ermöglicht es, Benutzersitzungen zu verfolgen, Einkaufswageninformationen zu speichern und Benutzereinstellungen beizubehalten, auch wenn der Nutzer nicht eingeloggt ist. Cookies tragen dazu bei, dass Benutzer bei einem erneuten Besuch einer Webseite personalisierte Inhalte oder einen gefüllten Warenkorb vorfinden, ohne sich erneut anmelden zu müssen.

1. **Name und Wert:** Jedes Cookie hat einen eindeutigen Namen und einen zugehörigen Wert. Diese Namen-Wert-Paare enthalten spezifische Informationen, die der Server über den Client speichern möchte.

2. **Domain und Pfad:** Diese definieren den Geltungsbereich des Cookies, d.h., für welche Domain (Webseite) und Pfad das Cookie gesetzt ist. Nur Anfragen an diese spezifische Domain und Pfad führen dazu, dass das Cookie vom Browser mitgesendet wird.

3. **Ablaufdatum:** Dies gibt an, wie lange das Cookie gültig sein soll. Wenn kein Ablaufdatum gesetzt ist, wird das Cookie typischerweise nach Beenden der Browsersession gelöscht.

4. **Sicherheitsflag:** Eine optionale Angabe, die festlegt, ob das Cookie nur über sichere Verbindungen (z.B. SSL/TLS) gesendet werden soll. Dies erhöht die Sicherheit, indem sichergestellt wird, dass das Cookie nicht über unverschlüsselte Verbindungen übertragen wird.

Cookies sind zwar nützlich für diverse Anwendungszwecke, haben aber auch bestimmte Nachteile:

1. **Datenvolumen**: Cookies werden mit jeder HTTP-Anfrage an die entsprechende Domain gesendet, was das Datenvolumen erhöht, selbst wenn diese Zunahme minimal sein mag.

2. **Sicherheitsrisiken**: Cookies, die über das unverschlüsselte HTTP-Protokoll gesendet werden, sind unverschlüsselt und stellen daher ein Sicherheitsrisiko dar, da die übermittelten Informationen abgefangen werden könnten.

3. **Speicherbeschränkung**: Die Menge an Daten, die in Cookies gespeichert werden können, ist auf 4 Kilobyte begrenzt.


## Kapitel 20 - Webanwendungen absichern

### 20.4.2 Session Based Authentication

Session Based Authentication ist eine Methode zur Nutzerauthentifizierung, die im Gegensatz zur Basic Authentication steht, bei der die Credentials (Nutzername und Passwort) bei jeder Anfrage an den Server gesendet werden. Bei der Session Based Authentication wird eine Session erstellt, nachdem sich der Nutzer erfolgreich mit Nutzername und Passwort am Server angemeldet hat. Diese Session wird durch eine einzigartige Session-ID identifiziert. Diese ID wird an den Nutzer zurückgesendet und in Form eines Cookies im Browser gespeichert. Bei jeder weiteren Anfrage an den Server sendet der Browser das Cookie mit der Session-ID zurück an den Server. Der Server kann dann die im Cookie gespeicherte Session-ID mit den auf dem Server gespeicherten Session-Informationen abgleichen, um die Identität des Nutzers zu bestätigen. Dadurch müssen die Anmeldeinformationen nicht bei jeder Anfrage neu übertragen werden, was die Sicherheit erhöht.

> Bei der Session Based Authentication werder nach erfolgreicher Authentifizierung nihct die Credentials mit jeder Anfrage mmitgesendet, sondern eine Session-ID, über die der Server den Nutzer für die Gültigkeit der Session identifizieren kann.


## Kapitel 13 - Programmiersprachen auf der Serverseite verwenden

### 13.2.2 Objektorientierte Programmierung

In der objektorientierten Programmierung wird der Zustand von Objekten durch deren Eigenschaften beschrieben, und das Verhalten wird durch Methoden definiert. Es gibt vier grundlegende Prinzipien:

1. **Abstraktion**: Dies bezieht sich auf die Definition von Klassen oder Prototypen, die als Blaupausen für Objekte dienen. Klassen fassen das abstrakte Verhalten und gemeinsame Eigenschaften von Objekten zusammen.

2. **Datenkapselung**: Eigenschaften und Methoden werden in Klassen eingekapselt, sodass der interne Zustand von außen verborgen und geschützt ist.

3. **Vererbung**: Eigenschaften und Methoden einer Klasse können an andere Klassen weitergegeben werden.

4. **Polymorphie**: Objekte können je nach Kontext verschiedene Typen annehmen, was bedeutet, dass ein Objekt in verschiedenen Formen auftreten kann.

Zusammengefasst bilden diese Prinzipien die Grundlage für die Struktur und das Design in der objektorientierten Programmierung, wobei das Ziel ist, den Code wiederverwendbar, wartbar und erweiterbar zu gestalten.


## Kapitel 15 - Die Sprache PHP verwenden

### 15.6 Klassen und Objekte verwenden

#### 15.6.1 Klassen schreiben

In PHP, neben primitiven Datentypen und Variablen, kann man auch Klassen und Objekte verwenden. PHP unterstützt ein vollständiges Objektmodell, einschließlich Sichtbarkeit, Abstraktion und anderen fortgeschrittenen Features wie Interfaces und das Klonen von Objekten.

Eine Klasse in PHP ist eine Sammlung von Methoden und Eigenschaften und dient als Vorlage für die Erstellung von Objekten. Das folgende Beispiel zeigt eine Klasse namens `Book`, die mit öffentlichen Eigenschaften wie Titel, Preis, Autor und ISBN definiert ist. Innerhalb der Klasse `Book` gibt es auch eine Methode `printDescription()`, die die Eigenschaften des Autors und den Titel des Buches ausgibt. Die `$this`-Variable innerhalb einer Methode verweist auf das aktuelle Objekt, auf dessen Eigenschaften und Methoden zugegriffen werden kann.

Hier ist ein PHP-Code-Snippet, das die Klasse `Book` und die Methode `printDescription()` zeigt:

```php
<?php
class Book {
    public string $title;
    public float $price;
    public string $author;
    public string $isbn;

    public function printDescription() {
        echo $this->author . ": " . $this->title;
    }
}
?>
```

Mit diesem Code-Snippet kann ein `Book`-Objekt erstellt und dessen Beschreibung gedruckt werden, indem die `printDescription()`-Methode aufgerufen wird.

#### 15.6.2 Objekte erstellen

In PHP wird eine neue Instanz einer Klasse mit dem `new`-Schlüsselwort erstellt. Nach der Instanziierung können die Eigenschaften des Objekts gesetzt werden. Im Beispiel wird eine Instanz der Klasse `Book` erstellt und in die Variable `$schroedinger` gelegt. Die Eigenschaften `title`, `price`, `author` und `isbn` des Objekts `$schroedinger` werden danach mit entsprechenden Werten gefüllt. Schließlich wird die Methode `printDescription()` aufgerufen, die den Autor und den Titel des Buches ausgibt. Der Zugriff auf Eigenschaften und Methoden des Objekts erfolgt mittels des Pfeiloperators `->`.

Hier ist ein entsprechendes PHP-Code-Snippet:

```php
<?php
class Book {
    public string $title;
    public float $price;
    public string $author;
    public string $isbn;

    public function printDescription() {
        echo $this->author . ": " . $this->title;
    }
}

$schroedinger = new Book();
$schroedinger->title = "Schrödinger programmiert Java";
$schroedinger->price = 44.90;
$schroedinger->author = "Philip Ackermann";
$schroedinger->isbn = "978-383624583798-3836245838";

$schroedinger->printDescription(); // Ausgabe: Philip Ackermann: Schrödinger programmiert Java
?>
```

Dieses Snippet zeigt, wie man ein Objekt in PHP erstellt, ihm Eigenschaften zuweist und eine Methode aufruft.

#### 15.6.3 Klassenkonstanten

In PHP können Klassen auch Konstanten beinhalten, die während der Laufzeit unveränderlich sind. Im Beispiel wird gezeigt, wie man eine Klassenkonstante definiert und verwendet. Innerhalb der Klasse `Book` wird eine Konstante `PROFIT` mit dem Wert `1.05` definiert. Die Methode `printPriceIncludingProfit()` innerhalb der Klasse nutzt diese Konstante, um den Preis inklusive Gewinn zu berechnen und auszugeben, indem der Preis mit dem Wert der Konstante multipliziert wird. Der Zugriff auf eine Klassenkonstante innerhalb einer Methode erfolgt durch die Verwendung des Schlüsselwortes `self` gefolgt vom Gültigkeitsbereichsoperator `::`. Um auf die Konstante von außerhalb der Klasse zuzugreifen, verwendet man den Klassennamen gefolgt vom Gültigkeitsbereichsoperator und dem Konstantennamen.

Hier ist ein PHP-Code-Snippet, das die Verwendung einer Klassenkonstante zeigt:

```php
<?php
class Book {
    public float $price;
    const PROFIT = 1.05;

    public function printPriceIncludingProfit() {
        echo $this->price * self::PROFIT;
    }
}

$schroedinger = new Book();
$schroedinger->price = 44.90;
$schroedinger->printPriceIncludingProfit(); // Ausgabe: Preis inklusive Gewinn

$profit = Book::PROFIT; // Zugriff auf die Konstante von außerhalb der Klasse
?>
```

In diesem Snippet wird ein `Book`-Objekt erstellt, der Preis gesetzt und dann die Methode `printPriceIncludingProfit()` aufgerufen, die den Preis inklusive des definierten Gewinnzuschlags ausgibt. Die Konstante `PROFIT` wird auch außerhalb der Klasse abgerufen und in der Variable `$profit` gespeichert.

#### 15.6.4 Sichtbarkeit

Sichtbarkeit legt fest, von wo auf Klasseneigenschaften und -methoden zugegriffen werden darf, mit den Schlüsselwörtern `public`, `protected` und `private`. 

- `public` bedeutet, dass von überall auf die Elemente zugegriffen werden kann.
- `private` Elemente sind nur innerhalb der Klasse selbst zugänglich.
- `protected` Elemente sind ähnlich wie `private` Elemente, können aber zusätzlich in abgeleiteten Klassen verwendet werden.

Im gegebenen Beispiel ist die Eigenschaft `price` der Klasse `Book` als `private` deklariert. Das bedeutet, dass sie nicht direkt von außen abgerufen oder verändert werden kann, wie es der Kommentar `//$schroedinger->price = 44.90;` impliziert, der nicht funktionieren würde. Um den Preis festzulegen, wird eine öffentliche Methode `setPrice()` definiert, die es erlaubt, den Preis von außerhalb der Klasse zu ändern.

Hier ist ein Code-Snippet, das die Verwendung der Sichtbarkeit und der `setPrice()`-Methode zeigt:

```php
<?php
class Book {
    const PROFIT = 1.05;
    public string $title;
    private float $price; // Preis ist als privat deklariert
    public string $author;
    public string $isbn;

    // Öffentliche Methode zum Setzen des Preises
    public function setPrice(float $price) {
        $this->price = $price;
    }

    // Methode zum Abrufen des Preises
    public function getPrice() {
        return $this->price;
    }
}

$schroedinger = new Book();
$schroedinger->title = "Schrödinger programmiert Java";
$schroedinger->setPrice(44.90); // Setzt den Preis über eine Methode
$price = $schroedinger->getPrice(); // Abrufen des Preises über eine Methode
echo $price; // Ausgabe des Preises
?>
```

In diesem Snippet wird der Preis des Buches über die `setPrice()`-Methode gesetzt und dann über eine weitere Methode `getPrice()` abgerufen, da ein direkter Zugriff auf `price` nicht möglich ist, weil es privat ist.

#### 15.6.5 Vererbung

Vererbung ermöglicht es einer Unterklasse (abgeleitete Klasse), Methoden, Eigenschaften und Konstanten von einer Oberklasse (Elternklasse) zu erben. Bei Vererbung wird zwischen den Klassen eine Hierarchie aufgebaut. In PHP wird Vererbung mit dem Schlüsselwort `extends` implementiert.

Die Unterklasse kann Methoden der Oberklasse überschreiben, was im Beispiel mit der Methode `printDescription()` in der Klasse `EBook` gezeigt wird, die eine spezialisierte Ausgabe für E-Books erzeugt, indem sie zunächst die Methode `printDescription()` der Elternklasse `Book` aufruft und dann zusätzliche Informationen hinzufügt.

Außerdem zeigt das Beispiel, dass der direkte Zugriff auf eine als `private` deklarierte Eigenschaft der Oberklasse (`price`) in der Unterklasse nicht möglich ist, wie der auskommentierte Code `//echo $this->price;` darstellt. Um den Preis in der Unterklasse zu manipulieren, muss eine öffentliche oder geschützte (`protected`) Methode verwendet werden, die in der Oberklasse definiert ist.

Hier ist ein Code-Snippet, das Vererbung und die Überschreibung einer Methode in PHP illustriert:

```php
<?php
class Book {
    const PROFIT = 1.05;
    public string $title;
    private float $price;
    public string $author;
    public string $isbn;

    public function setPrice(float $price) {
        $this->price = $price;
    }

    public function printDescription() {
        echo $this->author . ": " . $this->title;
    }
}

class EBook extends Book {
    public function printDescription() {
        parent::printDescription(); // Aufruf der Elternmethode
        echo " (E-Book Version)";
    }

    // Diese Methode ist erforderlich, um auf die private Eigenschaft price zuzugreifen
    public function printPrice() {
        // Zugriff auf price über eine Methode in der Elternklasse
        echo $this->getPrice();
    }

    // Methode in der Elternklasse zum Abrufen des Preises
    protected function getPrice() {
        return $this->price;
    }
}

$schroedinger = new EBook();
$schroedinger->title = "Schrödinger programmiert Java";
$schroedinger->author = "Philip Ackermann";
$schroedinger->setPrice(39.90);
$schroedinger->printDescription(); // Ausgabe: Philip Ackermann: Schrödinger programmiert Java (E-Book Version)
$schroedinger->printPrice(); // Ausgabe des Preises
?>
```

In diesem Snippet wird die Methode `printDescription()` in der Unterklasse `EBook` überschrieben, um spezifische Informationen für E-Books hinzuzufügen. Die Methode `printPrice()` in der Unterklasse `EBook` bietet einen Weg, um den Preis auszugeben, der als `private` in der Oberklasse `Book` definiert ist.

#### 15.6.6 Klassenabstraktion

Eine abstrakte Klasse ist eine, die mit dem Schlüsselwort `abstract` definiert wird und dazu dient, eine Basisklasse für andere Klassen zu sein, ohne selbst instanziiert zu werden. Sie kann abstrakte Methoden enthalten, die keine Implementierung haben und somit müssen die abgeleiteten Klassen diese Methoden implementieren.

Das Schlüsselwort `abstract` vor einer Methode weist darauf hin, dass die Methode in der abstrakten Klasse keine Implementierung hat und in jeder abgeleiteten Klasse definiert werden muss. Abstrakte Klassen können auch nicht-abstrakte (konkrete) Methoden haben, die eine Standardimplementierung bieten, die in abgeleiteten Klassen verwendet oder überschrieben werden kann.

Hier ist ein Beispiel für PHP-Code, der eine abstrakte Klasse und eine von dieser abgeleitete Klasse zeigt:

```php
<?php
// Definition der abstrakten Klasse
abstract class AbstractBook {
    private float $price;
    
    // Abstrakte Methode, die in der abgeleiteten Klasse implementiert werden muss
    abstract public function printDescription();

    // Konkrete Methode, die bereits implementiert ist
    public function setPrice(float $price) {
        $this->price = $price;
    }
    
    // Eine Möglichkeit, auf den privaten Preis zuzugreifen
    protected function getPrice() {
        return $this->price;
    }
}

// Abgeleitete Klasse, die die abstrakte Klasse erweitert
class Book extends AbstractBook {
    public string $title;
    public string $author;

    // Implementierung der abstrakten Methode aus der Elternklasse
    public function printDescription() {
        echo $this->author . ": " . $this->title;
    }
    
    // Eine zusätzliche Methode, um den Preis inklusive Beschreibung auszugeben
    public function printPriceDescription() {
        echo $this->author . ": " . $this->title . " - Price: " . $this->getPrice();
    }
}

// Instanziierung der abgeleiteten Klasse
$myBook = new Book();
$myBook->title = "The PHP Way";
$myBook->author = "John Doe";
$myBook->setPrice(29.99);

// Aufruf der Methoden
$myBook->printDescription(); // Gibt die Beschreibung aus
$myBook->printPriceDescription(); // Gibt Preis und Beschreibung aus
?>
```

In diesem Snippet wird die abstrakte Klasse `AbstractBook` mit einer abstrakten Methode `printDescription` und einer konkreten Methode `setPrice` definiert. Die Klasse `Book` erbt von `AbstractBook` und implementiert die abstrakte Methode. Die Methode `printPriceDescription` in `Book` gibt sowohl die Beschreibung als auch den Preis des Buches aus.


## Kapitel 6

### 6.1.3 JSON

JSON (JavaScript Object Notation) ist ein leichtgewichtiges Daten-Austauschformat, das für Menschen leicht zu lesen und zu schreiben und für Maschinen einfach zu parsen (analysieren) und zu generieren ist. JSON basiert auf einer Teilmenge der JavaScript-Programmiersprache, Standard ECMA-262 3. Ausgabe - Dezember 1999. Es wird häufig verwendet, um Daten zwischen einem Server und einer Webanwendung zu übertragen.

Hier sind die Grundkonzepte von JSON:

1. **Struktur**: JSON ist strukturiert als Sammlung von Schlüssel-Wert-Paaren (ähnlich einem Objekt in JavaScript) oder als geordnete Liste von Werten (eine Array in JavaScript).

2. **Schlüssel-Wert-Paare**: Jeder Schlüssel ist ein String, gefolgt von einem Doppelpunkt, und der Wert kann ein String, eine Zahl, ein Boolescher Wert, ein Array, ein Objekt oder `null` sein.

3. **Arrays**: Eine geordnete Liste von Werten, die von eckigen Klammern umschlossen sind und durch Kommas getrennt werden.

4. **Daten Typen**: Zu den unterstützten Datentypen gehören:
   - String (muss in doppelten Anführungszeichen stehen)
   - Zahl (Integer oder Float)
   - Objekt (eine ungeordnete Sammlung von Schlüssel-Wert-Paaren)
   - Array (eine geordnete Sammlung von Werten)
   - Boolescher Wert (`true` oder `false`)
   - `null`

5. **Syntax**: JSON verwendet eine einfache Textformat-Syntax, die es ermöglicht, Datenstrukturen leicht in einer Zeichenkette zu kodieren und zu dekodieren.

6. **Kompatibilität**: Obwohl es aus JavaScript abgeleitet ist, ist JSON sprachunabhängig und Parser existieren für viele Programmiersprachen.

7. **Verwendung**: Es ist besonders nützlich für die Übertragung von Daten über Netzwerke und wird in Web Services und APIs weit verbreitet verwendet.

Hier ist ein Beispiel für einen JSON-String, der ein Objekt darstellt:

```json
{
  "name": "John Doe",
  "age": 30,
  "isStudent": false,
  "courses": ["Mathematics", "Science"],
  "address": {
    "street": "123 Main St",
    "city": "Anytown"
  }
}
```

In diesem Beispiel sind `name`, `age`, `isStudent`, `courses` und `address` Schlüssel. Die entsprechenden Werte sind ein String, eine Zahl, ein Boolescher Wert, ein Array und ein eingebettetes Objekt.



