# Docker Install PHP

Vorteile von Docker gegenüber XAMPP-Installation: lokales und remote Development, Portability, Einfachheit, Scalability, CD/CD-Unterstützung


### Installation von Docker

* Installationshilfe: https://docs.docker.com/toolbox/toolbox_install_windows/
* Voraussetzung für Windows: WSL2 aktivieren/installieren
* Nicht-Admin-User der Docker-Gruppe hinzufügen

### Installation überprüfen:
In der Docker-Konsole:
``` bash
docker run hello-world
```

docker-compose.yml herunterladen und ins Projektverzeichnis kopieren

Optional: Port-Mapping und Volume-Pfade anpassen


### Ausführen der Docker-Container
``` bash
docker-compose up
```

### Webserver
http://localhost:8083

### Datenbankzugriff
>phpmyadmin -> MACHINE-IP:8082
>
>Benutzer: root
>
>Passwort: jkluio

### Datenbank-Konsole:
```
docker exec -it mysql /usr/bin/mysql -uroot -pjkluio
```

## Datenbank manuell exportieren/importieren:

### Backup ALL Databases
```
docker exec mysql /usr/bin/mysqldump -uroot -pjkluio --all-databases --skip-lock-tables > backup.sql
```

### Backup SPECIFIC Databases
```
docker exec mysql /usr/bin/mysqldump -uroot -pjkluio --databases php12 php13 --skip-lock-tables > backup.sql
```

### Restore
```
cat backup.sql | docker exec -i mysql /usr/bin/mysql -uroot -pjkluio
```

---
---
---

# Workbench setup
### Create a new SQL connection
Hostname:
```
127.0.0.1
```
Port:
```
3306
```
Username:
```
root
```
Password:
```
jkluio
```

### Create a new Database
``` sql
CREATE DATABASE mydatabase;
```

### Switch to new Database
``` sql
USE mydatabase;
```

### Create the Table
``` sql
CREATE TABLE users (
    user_id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    is_active BOOLEAN
);
```

### Verify the Table Creation
``` sql
DESCRIBE users;
```

### INSERT Example
``` sql
INSERT INTO users (username, email, is_active) VALUES ('john_doe', 'john.doe@example.com', 1);
```

### Example Query
``` sql
SELECT email FROM users;
```

# Spinning up Docker

Go to path:
> C:\Users\paddi\gitlab\pos1-webdev\pos1-webdev\pos1-webdev

Dockerfile:

```
# Use an official PHP runtime as a parent image
FROM php:7.4-apache

# Set the working directory to /var/www/html
WORKDIR /var/www/html

# Copy all files from the current directory into the container
COPY . /var/www/html/

# Expose port 80 for Apache
EXPOSE 80

# Start Apache when the container runs
CMD ["apache2-foreground"]
```

Show running Docker

``` bash
docker ps
```

Build the Docker

```
docker build -t my-web-app .
```

Run the Docker

```
docker run -d -p 8080:80 my-web-app
```

In the Browser

```
http://localhost:8080
```
